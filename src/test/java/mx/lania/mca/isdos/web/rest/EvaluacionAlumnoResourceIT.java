package mx.lania.mca.isdos.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mx.lania.mca.isdos.IntegrationTest;
import mx.lania.mca.isdos.domain.EvaluacionAlumno;
import mx.lania.mca.isdos.repository.EvaluacionAlumnoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EvaluacionAlumnoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EvaluacionAlumnoResourceIT {

    private static final Double DEFAULT_RESULTADO = 1D;
    private static final Double UPDATED_RESULTADO = 2D;

    private static final String ENTITY_API_URL = "/api/evaluacion-alumnos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EvaluacionAlumnoRepository evaluacionAlumnoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEvaluacionAlumnoMockMvc;

    private EvaluacionAlumno evaluacionAlumno;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EvaluacionAlumno createEntity(EntityManager em) {
        EvaluacionAlumno evaluacionAlumno = new EvaluacionAlumno().resultado(DEFAULT_RESULTADO);
        return evaluacionAlumno;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EvaluacionAlumno createUpdatedEntity(EntityManager em) {
        EvaluacionAlumno evaluacionAlumno = new EvaluacionAlumno().resultado(UPDATED_RESULTADO);
        return evaluacionAlumno;
    }

    @BeforeEach
    public void initTest() {
        evaluacionAlumno = createEntity(em);
    }

    @Test
    @Transactional
    void createEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeCreate = evaluacionAlumnoRepository.findAll().size();
        // Create the EvaluacionAlumno
        restEvaluacionAlumnoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isCreated());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeCreate + 1);
        EvaluacionAlumno testEvaluacionAlumno = evaluacionAlumnoList.get(evaluacionAlumnoList.size() - 1);
        assertThat(testEvaluacionAlumno.getResultado()).isEqualTo(DEFAULT_RESULTADO);
    }

    @Test
    @Transactional
    void createEvaluacionAlumnoWithExistingId() throws Exception {
        // Create the EvaluacionAlumno with an existing ID
        evaluacionAlumno.setId(1L);

        int databaseSizeBeforeCreate = evaluacionAlumnoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvaluacionAlumnoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllEvaluacionAlumnos() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        // Get all the evaluacionAlumnoList
        restEvaluacionAlumnoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evaluacionAlumno.getId().intValue())))
            .andExpect(jsonPath("$.[*].resultado").value(hasItem(DEFAULT_RESULTADO.doubleValue())));
    }

    @Test
    @Transactional
    void getEvaluacionAlumno() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        // Get the evaluacionAlumno
        restEvaluacionAlumnoMockMvc
            .perform(get(ENTITY_API_URL_ID, evaluacionAlumno.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(evaluacionAlumno.getId().intValue()))
            .andExpect(jsonPath("$.resultado").value(DEFAULT_RESULTADO.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingEvaluacionAlumno() throws Exception {
        // Get the evaluacionAlumno
        restEvaluacionAlumnoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEvaluacionAlumno() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();

        // Update the evaluacionAlumno
        EvaluacionAlumno updatedEvaluacionAlumno = evaluacionAlumnoRepository.findById(evaluacionAlumno.getId()).get();
        // Disconnect from session so that the updates on updatedEvaluacionAlumno are not directly saved in db
        em.detach(updatedEvaluacionAlumno);
        updatedEvaluacionAlumno.resultado(UPDATED_RESULTADO);

        restEvaluacionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEvaluacionAlumno.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEvaluacionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        EvaluacionAlumno testEvaluacionAlumno = evaluacionAlumnoList.get(evaluacionAlumnoList.size() - 1);
        assertThat(testEvaluacionAlumno.getResultado()).isEqualTo(UPDATED_RESULTADO);
    }

    @Test
    @Transactional
    void putNonExistingEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, evaluacionAlumno.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEvaluacionAlumnoWithPatch() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();

        // Update the evaluacionAlumno using partial update
        EvaluacionAlumno partialUpdatedEvaluacionAlumno = new EvaluacionAlumno();
        partialUpdatedEvaluacionAlumno.setId(evaluacionAlumno.getId());

        restEvaluacionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEvaluacionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEvaluacionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        EvaluacionAlumno testEvaluacionAlumno = evaluacionAlumnoList.get(evaluacionAlumnoList.size() - 1);
        assertThat(testEvaluacionAlumno.getResultado()).isEqualTo(DEFAULT_RESULTADO);
    }

    @Test
    @Transactional
    void fullUpdateEvaluacionAlumnoWithPatch() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();

        // Update the evaluacionAlumno using partial update
        EvaluacionAlumno partialUpdatedEvaluacionAlumno = new EvaluacionAlumno();
        partialUpdatedEvaluacionAlumno.setId(evaluacionAlumno.getId());

        partialUpdatedEvaluacionAlumno.resultado(UPDATED_RESULTADO);

        restEvaluacionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEvaluacionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEvaluacionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        EvaluacionAlumno testEvaluacionAlumno = evaluacionAlumnoList.get(evaluacionAlumnoList.size() - 1);
        assertThat(testEvaluacionAlumno.getResultado()).isEqualTo(UPDATED_RESULTADO);
    }

    @Test
    @Transactional
    void patchNonExistingEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, evaluacionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEvaluacionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = evaluacionAlumnoRepository.findAll().size();
        evaluacionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEvaluacionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(evaluacionAlumno))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EvaluacionAlumno in the database
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEvaluacionAlumno() throws Exception {
        // Initialize the database
        evaluacionAlumnoRepository.saveAndFlush(evaluacionAlumno);

        int databaseSizeBeforeDelete = evaluacionAlumnoRepository.findAll().size();

        // Delete the evaluacionAlumno
        restEvaluacionAlumnoMockMvc
            .perform(delete(ENTITY_API_URL_ID, evaluacionAlumno.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EvaluacionAlumno> evaluacionAlumnoList = evaluacionAlumnoRepository.findAll();
        assertThat(evaluacionAlumnoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
