package mx.lania.mca.isdos.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mx.lania.mca.isdos.IntegrationTest;
import mx.lania.mca.isdos.domain.IteracionAlumno;
import mx.lania.mca.isdos.repository.IteracionAlumnoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link IteracionAlumnoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class IteracionAlumnoResourceIT {

    private static final String DEFAULT_ACTIVIDAD = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVIDAD = "BBBBBBBBBB";

    private static final Integer DEFAULT_TERMINADO = 1;
    private static final Integer UPDATED_TERMINADO = 2;

    private static final String ENTITY_API_URL = "/api/iteracion-alumnos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IteracionAlumnoRepository iteracionAlumnoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIteracionAlumnoMockMvc;

    private IteracionAlumno iteracionAlumno;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IteracionAlumno createEntity(EntityManager em) {
        IteracionAlumno iteracionAlumno = new IteracionAlumno().actividad(DEFAULT_ACTIVIDAD).terminado(DEFAULT_TERMINADO);
        return iteracionAlumno;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IteracionAlumno createUpdatedEntity(EntityManager em) {
        IteracionAlumno iteracionAlumno = new IteracionAlumno().actividad(UPDATED_ACTIVIDAD).terminado(UPDATED_TERMINADO);
        return iteracionAlumno;
    }

    @BeforeEach
    public void initTest() {
        iteracionAlumno = createEntity(em);
    }

    @Test
    @Transactional
    void createIteracionAlumno() throws Exception {
        int databaseSizeBeforeCreate = iteracionAlumnoRepository.findAll().size();
        // Create the IteracionAlumno
        restIteracionAlumnoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isCreated());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeCreate + 1);
        IteracionAlumno testIteracionAlumno = iteracionAlumnoList.get(iteracionAlumnoList.size() - 1);
        assertThat(testIteracionAlumno.getActividad()).isEqualTo(DEFAULT_ACTIVIDAD);
        assertThat(testIteracionAlumno.getTerminado()).isEqualTo(DEFAULT_TERMINADO);
    }

    @Test
    @Transactional
    void createIteracionAlumnoWithExistingId() throws Exception {
        // Create the IteracionAlumno with an existing ID
        iteracionAlumno.setId(1L);

        int databaseSizeBeforeCreate = iteracionAlumnoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restIteracionAlumnoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllIteracionAlumnos() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        // Get all the iteracionAlumnoList
        restIteracionAlumnoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(iteracionAlumno.getId().intValue())))
            .andExpect(jsonPath("$.[*].actividad").value(hasItem(DEFAULT_ACTIVIDAD)))
            .andExpect(jsonPath("$.[*].terminado").value(hasItem(DEFAULT_TERMINADO)));
    }

    @Test
    @Transactional
    void getIteracionAlumno() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        // Get the iteracionAlumno
        restIteracionAlumnoMockMvc
            .perform(get(ENTITY_API_URL_ID, iteracionAlumno.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(iteracionAlumno.getId().intValue()))
            .andExpect(jsonPath("$.actividad").value(DEFAULT_ACTIVIDAD))
            .andExpect(jsonPath("$.terminado").value(DEFAULT_TERMINADO));
    }

    @Test
    @Transactional
    void getNonExistingIteracionAlumno() throws Exception {
        // Get the iteracionAlumno
        restIteracionAlumnoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewIteracionAlumno() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();

        // Update the iteracionAlumno
        IteracionAlumno updatedIteracionAlumno = iteracionAlumnoRepository.findById(iteracionAlumno.getId()).get();
        // Disconnect from session so that the updates on updatedIteracionAlumno are not directly saved in db
        em.detach(updatedIteracionAlumno);
        updatedIteracionAlumno.actividad(UPDATED_ACTIVIDAD).terminado(UPDATED_TERMINADO);

        restIteracionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedIteracionAlumno.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedIteracionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        IteracionAlumno testIteracionAlumno = iteracionAlumnoList.get(iteracionAlumnoList.size() - 1);
        assertThat(testIteracionAlumno.getActividad()).isEqualTo(UPDATED_ACTIVIDAD);
        assertThat(testIteracionAlumno.getTerminado()).isEqualTo(UPDATED_TERMINADO);
    }

    @Test
    @Transactional
    void putNonExistingIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, iteracionAlumno.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateIteracionAlumnoWithPatch() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();

        // Update the iteracionAlumno using partial update
        IteracionAlumno partialUpdatedIteracionAlumno = new IteracionAlumno();
        partialUpdatedIteracionAlumno.setId(iteracionAlumno.getId());

        restIteracionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIteracionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIteracionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        IteracionAlumno testIteracionAlumno = iteracionAlumnoList.get(iteracionAlumnoList.size() - 1);
        assertThat(testIteracionAlumno.getActividad()).isEqualTo(DEFAULT_ACTIVIDAD);
        assertThat(testIteracionAlumno.getTerminado()).isEqualTo(DEFAULT_TERMINADO);
    }

    @Test
    @Transactional
    void fullUpdateIteracionAlumnoWithPatch() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();

        // Update the iteracionAlumno using partial update
        IteracionAlumno partialUpdatedIteracionAlumno = new IteracionAlumno();
        partialUpdatedIteracionAlumno.setId(iteracionAlumno.getId());

        partialUpdatedIteracionAlumno.actividad(UPDATED_ACTIVIDAD).terminado(UPDATED_TERMINADO);

        restIteracionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIteracionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIteracionAlumno))
            )
            .andExpect(status().isOk());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
        IteracionAlumno testIteracionAlumno = iteracionAlumnoList.get(iteracionAlumnoList.size() - 1);
        assertThat(testIteracionAlumno.getActividad()).isEqualTo(UPDATED_ACTIVIDAD);
        assertThat(testIteracionAlumno.getTerminado()).isEqualTo(UPDATED_TERMINADO);
    }

    @Test
    @Transactional
    void patchNonExistingIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, iteracionAlumno.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isBadRequest());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamIteracionAlumno() throws Exception {
        int databaseSizeBeforeUpdate = iteracionAlumnoRepository.findAll().size();
        iteracionAlumno.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIteracionAlumnoMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(iteracionAlumno))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the IteracionAlumno in the database
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteIteracionAlumno() throws Exception {
        // Initialize the database
        iteracionAlumnoRepository.saveAndFlush(iteracionAlumno);

        int databaseSizeBeforeDelete = iteracionAlumnoRepository.findAll().size();

        // Delete the iteracionAlumno
        restIteracionAlumnoMockMvc
            .perform(delete(ENTITY_API_URL_ID, iteracionAlumno.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IteracionAlumno> iteracionAlumnoList = iteracionAlumnoRepository.findAll();
        assertThat(iteracionAlumnoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
