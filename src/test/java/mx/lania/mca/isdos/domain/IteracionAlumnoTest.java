package mx.lania.mca.isdos.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mx.lania.mca.isdos.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class IteracionAlumnoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IteracionAlumno.class);
        IteracionAlumno iteracionAlumno1 = new IteracionAlumno();
        iteracionAlumno1.setId(1L);
        IteracionAlumno iteracionAlumno2 = new IteracionAlumno();
        iteracionAlumno2.setId(iteracionAlumno1.getId());
        assertThat(iteracionAlumno1).isEqualTo(iteracionAlumno2);
        iteracionAlumno2.setId(2L);
        assertThat(iteracionAlumno1).isNotEqualTo(iteracionAlumno2);
        iteracionAlumno1.setId(null);
        assertThat(iteracionAlumno1).isNotEqualTo(iteracionAlumno2);
    }
}
