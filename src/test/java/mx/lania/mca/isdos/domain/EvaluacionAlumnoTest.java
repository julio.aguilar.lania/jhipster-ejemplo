package mx.lania.mca.isdos.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mx.lania.mca.isdos.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EvaluacionAlumnoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvaluacionAlumno.class);
        EvaluacionAlumno evaluacionAlumno1 = new EvaluacionAlumno();
        evaluacionAlumno1.setId(1L);
        EvaluacionAlumno evaluacionAlumno2 = new EvaluacionAlumno();
        evaluacionAlumno2.setId(evaluacionAlumno1.getId());
        assertThat(evaluacionAlumno1).isEqualTo(evaluacionAlumno2);
        evaluacionAlumno2.setId(2L);
        assertThat(evaluacionAlumno1).isNotEqualTo(evaluacionAlumno2);
        evaluacionAlumno1.setId(null);
        assertThat(evaluacionAlumno1).isNotEqualTo(evaluacionAlumno2);
    }
}
