package mx.lania.mca.isdos.service.impl;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Equipo;
import mx.lania.mca.isdos.repository.EquipoRepository;
import mx.lania.mca.isdos.service.EquipoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Equipo}.
 */
@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

    private final Logger log = LoggerFactory.getLogger(EquipoServiceImpl.class);

    private final EquipoRepository equipoRepository;

    public EquipoServiceImpl(EquipoRepository equipoRepository) {
        this.equipoRepository = equipoRepository;
    }

    @Override
    public Equipo save(Equipo equipo) {
        log.debug("Request to save Equipo : {}", equipo);
        return equipoRepository.save(equipo);
    }

    @Override
    public Equipo update(Equipo equipo) {
        log.debug("Request to save Equipo : {}", equipo);
        return equipoRepository.save(equipo);
    }

    @Override
    public Optional<Equipo> partialUpdate(Equipo equipo) {
        log.debug("Request to partially update Equipo : {}", equipo);

        return equipoRepository
            .findById(equipo.getId())
            .map(existingEquipo -> {
                if (equipo.getNombre() != null) {
                    existingEquipo.setNombre(equipo.getNombre());
                }

                return existingEquipo;
            })
            .map(equipoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Equipo> findAll() {
        log.debug("Request to get all Equipos");
        return equipoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Equipo> findOne(Long id) {
        log.debug("Request to get Equipo : {}", id);
        return equipoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Equipo : {}", id);
        equipoRepository.deleteById(id);
    }
}
