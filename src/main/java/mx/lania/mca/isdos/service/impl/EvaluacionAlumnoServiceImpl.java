package mx.lania.mca.isdos.service.impl;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.EvaluacionAlumno;
import mx.lania.mca.isdos.repository.EvaluacionAlumnoRepository;
import mx.lania.mca.isdos.service.EvaluacionAlumnoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EvaluacionAlumno}.
 */
@Service
@Transactional
public class EvaluacionAlumnoServiceImpl implements EvaluacionAlumnoService {

    private final Logger log = LoggerFactory.getLogger(EvaluacionAlumnoServiceImpl.class);

    private final EvaluacionAlumnoRepository evaluacionAlumnoRepository;

    public EvaluacionAlumnoServiceImpl(EvaluacionAlumnoRepository evaluacionAlumnoRepository) {
        this.evaluacionAlumnoRepository = evaluacionAlumnoRepository;
    }

    @Override
    public EvaluacionAlumno save(EvaluacionAlumno evaluacionAlumno) {
        log.debug("Request to save EvaluacionAlumno : {}", evaluacionAlumno);
        return evaluacionAlumnoRepository.save(evaluacionAlumno);
    }

    @Override
    public EvaluacionAlumno update(EvaluacionAlumno evaluacionAlumno) {
        log.debug("Request to save EvaluacionAlumno : {}", evaluacionAlumno);
        return evaluacionAlumnoRepository.save(evaluacionAlumno);
    }

    @Override
    public Optional<EvaluacionAlumno> partialUpdate(EvaluacionAlumno evaluacionAlumno) {
        log.debug("Request to partially update EvaluacionAlumno : {}", evaluacionAlumno);

        return evaluacionAlumnoRepository
            .findById(evaluacionAlumno.getId())
            .map(existingEvaluacionAlumno -> {
                if (evaluacionAlumno.getResultado() != null) {
                    existingEvaluacionAlumno.setResultado(evaluacionAlumno.getResultado());
                }

                return existingEvaluacionAlumno;
            })
            .map(evaluacionAlumnoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EvaluacionAlumno> findAll() {
        log.debug("Request to get all EvaluacionAlumnos");
        return evaluacionAlumnoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EvaluacionAlumno> findOne(Long id) {
        log.debug("Request to get EvaluacionAlumno : {}", id);
        return evaluacionAlumnoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete EvaluacionAlumno : {}", id);
        evaluacionAlumnoRepository.deleteById(id);
    }
}
