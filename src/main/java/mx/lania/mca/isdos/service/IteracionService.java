package mx.lania.mca.isdos.service;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Iteracion;

/**
 * Service Interface for managing {@link Iteracion}.
 */
public interface IteracionService {
    /**
     * Save a iteracion.
     *
     * @param iteracion the entity to save.
     * @return the persisted entity.
     */
    Iteracion save(Iteracion iteracion);

    /**
     * Updates a iteracion.
     *
     * @param iteracion the entity to update.
     * @return the persisted entity.
     */
    Iteracion update(Iteracion iteracion);

    /**
     * Partially updates a iteracion.
     *
     * @param iteracion the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Iteracion> partialUpdate(Iteracion iteracion);

    /**
     * Get all the iteracions.
     *
     * @return the list of entities.
     */
    List<Iteracion> findAll();

    /**
     * Get the "id" iteracion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Iteracion> findOne(Long id);

    /**
     * Delete the "id" iteracion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
