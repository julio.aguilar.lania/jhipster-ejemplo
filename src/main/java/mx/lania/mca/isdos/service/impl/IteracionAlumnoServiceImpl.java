package mx.lania.mca.isdos.service.impl;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.IteracionAlumno;
import mx.lania.mca.isdos.repository.IteracionAlumnoRepository;
import mx.lania.mca.isdos.service.IteracionAlumnoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link IteracionAlumno}.
 */
@Service
@Transactional
public class IteracionAlumnoServiceImpl implements IteracionAlumnoService {

    private final Logger log = LoggerFactory.getLogger(IteracionAlumnoServiceImpl.class);

    private final IteracionAlumnoRepository iteracionAlumnoRepository;

    public IteracionAlumnoServiceImpl(IteracionAlumnoRepository iteracionAlumnoRepository) {
        this.iteracionAlumnoRepository = iteracionAlumnoRepository;
    }

    @Override
    public IteracionAlumno save(IteracionAlumno iteracionAlumno) {
        log.debug("Request to save IteracionAlumno : {}", iteracionAlumno);
        return iteracionAlumnoRepository.save(iteracionAlumno);
    }

    @Override
    public IteracionAlumno update(IteracionAlumno iteracionAlumno) {
        log.debug("Request to save IteracionAlumno : {}", iteracionAlumno);
        return iteracionAlumnoRepository.save(iteracionAlumno);
    }

    @Override
    public Optional<IteracionAlumno> partialUpdate(IteracionAlumno iteracionAlumno) {
        log.debug("Request to partially update IteracionAlumno : {}", iteracionAlumno);

        return iteracionAlumnoRepository
            .findById(iteracionAlumno.getId())
            .map(existingIteracionAlumno -> {
                if (iteracionAlumno.getActividad() != null) {
                    existingIteracionAlumno.setActividad(iteracionAlumno.getActividad());
                }
                if (iteracionAlumno.getTerminado() != null) {
                    existingIteracionAlumno.setTerminado(iteracionAlumno.getTerminado());
                }

                return existingIteracionAlumno;
            })
            .map(iteracionAlumnoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<IteracionAlumno> findAll() {
        log.debug("Request to get all IteracionAlumnos");
        return iteracionAlumnoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<IteracionAlumno> findOne(Long id) {
        log.debug("Request to get IteracionAlumno : {}", id);
        return iteracionAlumnoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete IteracionAlumno : {}", id);
        iteracionAlumnoRepository.deleteById(id);
    }
}
