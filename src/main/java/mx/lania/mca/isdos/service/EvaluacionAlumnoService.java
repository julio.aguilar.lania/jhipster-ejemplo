package mx.lania.mca.isdos.service;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.EvaluacionAlumno;

/**
 * Service Interface for managing {@link EvaluacionAlumno}.
 */
public interface EvaluacionAlumnoService {
    /**
     * Save a evaluacionAlumno.
     *
     * @param evaluacionAlumno the entity to save.
     * @return the persisted entity.
     */
    EvaluacionAlumno save(EvaluacionAlumno evaluacionAlumno);

    /**
     * Updates a evaluacionAlumno.
     *
     * @param evaluacionAlumno the entity to update.
     * @return the persisted entity.
     */
    EvaluacionAlumno update(EvaluacionAlumno evaluacionAlumno);

    /**
     * Partially updates a evaluacionAlumno.
     *
     * @param evaluacionAlumno the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EvaluacionAlumno> partialUpdate(EvaluacionAlumno evaluacionAlumno);

    /**
     * Get all the evaluacionAlumnos.
     *
     * @return the list of entities.
     */
    List<EvaluacionAlumno> findAll();

    /**
     * Get the "id" evaluacionAlumno.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EvaluacionAlumno> findOne(Long id);

    /**
     * Delete the "id" evaluacionAlumno.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
