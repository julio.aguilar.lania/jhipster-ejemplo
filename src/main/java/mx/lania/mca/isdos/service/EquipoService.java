package mx.lania.mca.isdos.service;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Equipo;

/**
 * Service Interface for managing {@link Equipo}.
 */
public interface EquipoService {
    /**
     * Save a equipo.
     *
     * @param equipo the entity to save.
     * @return the persisted entity.
     */
    Equipo save(Equipo equipo);

    /**
     * Updates a equipo.
     *
     * @param equipo the entity to update.
     * @return the persisted entity.
     */
    Equipo update(Equipo equipo);

    /**
     * Partially updates a equipo.
     *
     * @param equipo the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Equipo> partialUpdate(Equipo equipo);

    /**
     * Get all the equipos.
     *
     * @return the list of entities.
     */
    List<Equipo> findAll();

    /**
     * Get the "id" equipo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Equipo> findOne(Long id);

    /**
     * Delete the "id" equipo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
