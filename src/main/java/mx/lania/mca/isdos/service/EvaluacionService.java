package mx.lania.mca.isdos.service;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Evaluacion;

/**
 * Service Interface for managing {@link Evaluacion}.
 */
public interface EvaluacionService {
    /**
     * Save a evaluacion.
     *
     * @param evaluacion the entity to save.
     * @return the persisted entity.
     */
    Evaluacion save(Evaluacion evaluacion);

    /**
     * Updates a evaluacion.
     *
     * @param evaluacion the entity to update.
     * @return the persisted entity.
     */
    Evaluacion update(Evaluacion evaluacion);

    /**
     * Partially updates a evaluacion.
     *
     * @param evaluacion the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Evaluacion> partialUpdate(Evaluacion evaluacion);

    /**
     * Get all the evaluacions.
     *
     * @return the list of entities.
     */
    List<Evaluacion> findAll();

    /**
     * Get the "id" evaluacion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Evaluacion> findOne(Long id);

    /**
     * Delete the "id" evaluacion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
