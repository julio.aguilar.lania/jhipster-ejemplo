package mx.lania.mca.isdos.service.impl;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Evaluacion;
import mx.lania.mca.isdos.repository.EvaluacionRepository;
import mx.lania.mca.isdos.service.EvaluacionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Evaluacion}.
 */
@Service
@Transactional
public class EvaluacionServiceImpl implements EvaluacionService {

    private final Logger log = LoggerFactory.getLogger(EvaluacionServiceImpl.class);

    private final EvaluacionRepository evaluacionRepository;

    public EvaluacionServiceImpl(EvaluacionRepository evaluacionRepository) {
        this.evaluacionRepository = evaluacionRepository;
    }

    @Override
    public Evaluacion save(Evaluacion evaluacion) {
        log.debug("Request to save Evaluacion : {}", evaluacion);
        return evaluacionRepository.save(evaluacion);
    }

    @Override
    public Evaluacion update(Evaluacion evaluacion) {
        log.debug("Request to save Evaluacion : {}", evaluacion);
        return evaluacionRepository.save(evaluacion);
    }

    @Override
    public Optional<Evaluacion> partialUpdate(Evaluacion evaluacion) {
        log.debug("Request to partially update Evaluacion : {}", evaluacion);

        return evaluacionRepository
            .findById(evaluacion.getId())
            .map(existingEvaluacion -> {
                if (evaluacion.getFecha() != null) {
                    existingEvaluacion.setFecha(evaluacion.getFecha());
                }

                return existingEvaluacion;
            })
            .map(evaluacionRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Evaluacion> findAll() {
        log.debug("Request to get all Evaluacions");
        return evaluacionRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Evaluacion> findOne(Long id) {
        log.debug("Request to get Evaluacion : {}", id);
        return evaluacionRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Evaluacion : {}", id);
        evaluacionRepository.deleteById(id);
    }
}
