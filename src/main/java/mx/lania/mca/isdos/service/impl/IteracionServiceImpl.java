package mx.lania.mca.isdos.service.impl;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.Iteracion;
import mx.lania.mca.isdos.repository.IteracionRepository;
import mx.lania.mca.isdos.service.IteracionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Iteracion}.
 */
@Service
@Transactional
public class IteracionServiceImpl implements IteracionService {

    private final Logger log = LoggerFactory.getLogger(IteracionServiceImpl.class);

    private final IteracionRepository iteracionRepository;

    public IteracionServiceImpl(IteracionRepository iteracionRepository) {
        this.iteracionRepository = iteracionRepository;
    }

    @Override
    public Iteracion save(Iteracion iteracion) {
        log.debug("Request to save Iteracion : {}", iteracion);
        return iteracionRepository.save(iteracion);
    }

    @Override
    public Iteracion update(Iteracion iteracion) {
        log.debug("Request to save Iteracion : {}", iteracion);
        return iteracionRepository.save(iteracion);
    }

    @Override
    public Optional<Iteracion> partialUpdate(Iteracion iteracion) {
        log.debug("Request to partially update Iteracion : {}", iteracion);

        return iteracionRepository
            .findById(iteracion.getId())
            .map(existingIteracion -> {
                if (iteracion.getFechaInicio() != null) {
                    existingIteracion.setFechaInicio(iteracion.getFechaInicio());
                }
                if (iteracion.getFechaFin() != null) {
                    existingIteracion.setFechaFin(iteracion.getFechaFin());
                }

                return existingIteracion;
            })
            .map(iteracionRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Iteracion> findAll() {
        log.debug("Request to get all Iteracions");
        return iteracionRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Iteracion> findOne(Long id) {
        log.debug("Request to get Iteracion : {}", id);
        return iteracionRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Iteracion : {}", id);
        iteracionRepository.deleteById(id);
    }
}
