package mx.lania.mca.isdos.service;

import java.util.List;
import java.util.Optional;
import mx.lania.mca.isdos.domain.IteracionAlumno;

/**
 * Service Interface for managing {@link IteracionAlumno}.
 */
public interface IteracionAlumnoService {
    /**
     * Save a iteracionAlumno.
     *
     * @param iteracionAlumno the entity to save.
     * @return the persisted entity.
     */
    IteracionAlumno save(IteracionAlumno iteracionAlumno);

    /**
     * Updates a iteracionAlumno.
     *
     * @param iteracionAlumno the entity to update.
     * @return the persisted entity.
     */
    IteracionAlumno update(IteracionAlumno iteracionAlumno);

    /**
     * Partially updates a iteracionAlumno.
     *
     * @param iteracionAlumno the entity to update partially.
     * @return the persisted entity.
     */
    Optional<IteracionAlumno> partialUpdate(IteracionAlumno iteracionAlumno);

    /**
     * Get all the iteracionAlumnos.
     *
     * @return the list of entities.
     */
    List<IteracionAlumno> findAll();

    /**
     * Get the "id" iteracionAlumno.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<IteracionAlumno> findOne(Long id);

    /**
     * Delete the "id" iteracionAlumno.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
