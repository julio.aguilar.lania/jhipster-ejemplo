package mx.lania.mca.isdos.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A IteracionAlumno.
 */
@Entity
@Table(name = "iteracion_alumno")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class IteracionAlumno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "actividad")
    private String actividad;

    @Column(name = "terminado")
    private Integer terminado;

    @ManyToOne
    private Iteracion iteracion;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equipo" }, allowSetters = true)
    private Alumno alumno;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public IteracionAlumno id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActividad() {
        return this.actividad;
    }

    public IteracionAlumno actividad(String actividad) {
        this.setActividad(actividad);
        return this;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Integer getTerminado() {
        return this.terminado;
    }

    public IteracionAlumno terminado(Integer terminado) {
        this.setTerminado(terminado);
        return this;
    }

    public void setTerminado(Integer terminado) {
        this.terminado = terminado;
    }

    public Iteracion getIteracion() {
        return this.iteracion;
    }

    public void setIteracion(Iteracion iteracion) {
        this.iteracion = iteracion;
    }

    public IteracionAlumno iteracion(Iteracion iteracion) {
        this.setIteracion(iteracion);
        return this;
    }

    public Alumno getAlumno() {
        return this.alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public IteracionAlumno alumno(Alumno alumno) {
        this.setAlumno(alumno);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IteracionAlumno)) {
            return false;
        }
        return id != null && id.equals(((IteracionAlumno) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IteracionAlumno{" +
            "id=" + getId() +
            ", actividad='" + getActividad() + "'" +
            ", terminado=" + getTerminado() +
            "}";
    }
}
