package mx.lania.mca.isdos.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EvaluacionAlumno.
 */
@Entity
@Table(name = "evaluacion_alumno")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EvaluacionAlumno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "resultado")
    private Double resultado;

    @ManyToOne
    private Evaluacion evaluacion;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equipo" }, allowSetters = true)
    private Alumno alumno;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equipo" }, allowSetters = true)
    private Alumno evaluador;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public EvaluacionAlumno id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getResultado() {
        return this.resultado;
    }

    public EvaluacionAlumno resultado(Double resultado) {
        this.setResultado(resultado);
        return this;
    }

    public void setResultado(Double resultado) {
        this.resultado = resultado;
    }

    public Evaluacion getEvaluacion() {
        return this.evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public EvaluacionAlumno evaluacion(Evaluacion evaluacion) {
        this.setEvaluacion(evaluacion);
        return this;
    }

    public Alumno getAlumno() {
        return this.alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public EvaluacionAlumno alumno(Alumno alumno) {
        this.setAlumno(alumno);
        return this;
    }

    public Alumno getEvaluador() {
        return this.evaluador;
    }

    public void setEvaluador(Alumno alumno) {
        this.evaluador = alumno;
    }

    public EvaluacionAlumno evaluador(Alumno alumno) {
        this.setEvaluador(alumno);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EvaluacionAlumno)) {
            return false;
        }
        return id != null && id.equals(((EvaluacionAlumno) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EvaluacionAlumno{" +
            "id=" + getId() +
            ", resultado=" + getResultado() +
            "}";
    }
}
