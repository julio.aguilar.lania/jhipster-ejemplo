package mx.lania.mca.isdos.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import mx.lania.mca.isdos.domain.IteracionAlumno;
import mx.lania.mca.isdos.repository.IteracionAlumnoRepository;
import mx.lania.mca.isdos.service.IteracionAlumnoService;
import mx.lania.mca.isdos.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mx.lania.mca.isdos.domain.IteracionAlumno}.
 */
@RestController
@RequestMapping("/api")
public class IteracionAlumnoResource {

    private final Logger log = LoggerFactory.getLogger(IteracionAlumnoResource.class);

    private static final String ENTITY_NAME = "iteracionAlumno";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IteracionAlumnoService iteracionAlumnoService;

    private final IteracionAlumnoRepository iteracionAlumnoRepository;

    public IteracionAlumnoResource(IteracionAlumnoService iteracionAlumnoService, IteracionAlumnoRepository iteracionAlumnoRepository) {
        this.iteracionAlumnoService = iteracionAlumnoService;
        this.iteracionAlumnoRepository = iteracionAlumnoRepository;
    }

    /**
     * {@code POST  /iteracion-alumnos} : Create a new iteracionAlumno.
     *
     * @param iteracionAlumno the iteracionAlumno to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new iteracionAlumno, or with status {@code 400 (Bad Request)} if the iteracionAlumno has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/iteracion-alumnos")
    public ResponseEntity<IteracionAlumno> createIteracionAlumno(@RequestBody IteracionAlumno iteracionAlumno) throws URISyntaxException {
        log.debug("REST request to save IteracionAlumno : {}", iteracionAlumno);
        if (iteracionAlumno.getId() != null) {
            throw new BadRequestAlertException("A new iteracionAlumno cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IteracionAlumno result = iteracionAlumnoService.save(iteracionAlumno);
        return ResponseEntity
            .created(new URI("/api/iteracion-alumnos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /iteracion-alumnos/:id} : Updates an existing iteracionAlumno.
     *
     * @param id the id of the iteracionAlumno to save.
     * @param iteracionAlumno the iteracionAlumno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iteracionAlumno,
     * or with status {@code 400 (Bad Request)} if the iteracionAlumno is not valid,
     * or with status {@code 500 (Internal Server Error)} if the iteracionAlumno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/iteracion-alumnos/{id}")
    public ResponseEntity<IteracionAlumno> updateIteracionAlumno(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody IteracionAlumno iteracionAlumno
    ) throws URISyntaxException {
        log.debug("REST request to update IteracionAlumno : {}, {}", id, iteracionAlumno);
        if (iteracionAlumno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iteracionAlumno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!iteracionAlumnoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        IteracionAlumno result = iteracionAlumnoService.update(iteracionAlumno);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, iteracionAlumno.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /iteracion-alumnos/:id} : Partial updates given fields of an existing iteracionAlumno, field will ignore if it is null
     *
     * @param id the id of the iteracionAlumno to save.
     * @param iteracionAlumno the iteracionAlumno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iteracionAlumno,
     * or with status {@code 400 (Bad Request)} if the iteracionAlumno is not valid,
     * or with status {@code 404 (Not Found)} if the iteracionAlumno is not found,
     * or with status {@code 500 (Internal Server Error)} if the iteracionAlumno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/iteracion-alumnos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<IteracionAlumno> partialUpdateIteracionAlumno(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody IteracionAlumno iteracionAlumno
    ) throws URISyntaxException {
        log.debug("REST request to partial update IteracionAlumno partially : {}, {}", id, iteracionAlumno);
        if (iteracionAlumno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iteracionAlumno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!iteracionAlumnoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<IteracionAlumno> result = iteracionAlumnoService.partialUpdate(iteracionAlumno);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, iteracionAlumno.getId().toString())
        );
    }

    /**
     * {@code GET  /iteracion-alumnos} : get all the iteracionAlumnos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of iteracionAlumnos in body.
     */
    @GetMapping("/iteracion-alumnos")
    public List<IteracionAlumno> getAllIteracionAlumnos() {
        log.debug("REST request to get all IteracionAlumnos");
        return iteracionAlumnoService.findAll();
    }

    /**
     * {@code GET  /iteracion-alumnos/:id} : get the "id" iteracionAlumno.
     *
     * @param id the id of the iteracionAlumno to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the iteracionAlumno, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/iteracion-alumnos/{id}")
    public ResponseEntity<IteracionAlumno> getIteracionAlumno(@PathVariable Long id) {
        log.debug("REST request to get IteracionAlumno : {}", id);
        Optional<IteracionAlumno> iteracionAlumno = iteracionAlumnoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(iteracionAlumno);
    }

    /**
     * {@code DELETE  /iteracion-alumnos/:id} : delete the "id" iteracionAlumno.
     *
     * @param id the id of the iteracionAlumno to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/iteracion-alumnos/{id}")
    public ResponseEntity<Void> deleteIteracionAlumno(@PathVariable Long id) {
        log.debug("REST request to delete IteracionAlumno : {}", id);
        iteracionAlumnoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
