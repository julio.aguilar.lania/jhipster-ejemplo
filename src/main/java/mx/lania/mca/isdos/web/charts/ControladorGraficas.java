package mx.lania.mca.isdos.web.charts;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Julio
 */
@Controller
public class ControladorGraficas {

    Logger LOGGER = LoggerFactory.getLogger(ControladorGraficas.class);

    @Autowired
    CreadorReportePDF reportePDF;
    
    @Autowired
    CreadorGraficas graficador;

    @GetMapping(value = "/pdf1", produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] generarPDF(@RequestParam("id") Integer id) {
        LOGGER.debug("GET reporte 1 {}", id);
        try {
            return reportePDF.crearPDFConDosCharts();
        } catch (IOException ex) {
            LOGGER.error("PDF 1", ex);
            return null;
        }
    }

    @GetMapping(value = "/pdf2", produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] generarPDF() {
        LOGGER.debug("GET reporte 2");
        try {
            return reportePDF.crearPDFConCuatroCharts();
        } catch (IOException ex) {
            LOGGER.error("PDF 1", ex);
            return null;
        }
    }
    
    @GetMapping(value = "/chart", produces = MediaType.IMAGE_PNG_VALUE)
    public @ResponseBody byte[] generarChart() {
        LOGGER.debug("GET chart");
        BufferedImage bi = graficador.graficaPastel(5, "Pastel", 800, 600);
        ByteArrayOutputStream salida = new ByteArrayOutputStream();
        try {
            ImageIO.write(bi, "png", salida);
            return salida.toByteArray();
        } catch (IOException ex) {
            LOGGER.error("Error de I/O:", ex);
            return null;
        }
    }

}
