package mx.lania.mca.isdos.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import mx.lania.mca.isdos.domain.EvaluacionAlumno;
import mx.lania.mca.isdos.repository.EvaluacionAlumnoRepository;
import mx.lania.mca.isdos.service.EvaluacionAlumnoService;
import mx.lania.mca.isdos.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mx.lania.mca.isdos.domain.EvaluacionAlumno}.
 */
@RestController
@RequestMapping("/api")
public class EvaluacionAlumnoResource {

    private final Logger log = LoggerFactory.getLogger(EvaluacionAlumnoResource.class);

    private static final String ENTITY_NAME = "evaluacionAlumno";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EvaluacionAlumnoService evaluacionAlumnoService;

    private final EvaluacionAlumnoRepository evaluacionAlumnoRepository;

    public EvaluacionAlumnoResource(
        EvaluacionAlumnoService evaluacionAlumnoService,
        EvaluacionAlumnoRepository evaluacionAlumnoRepository
    ) {
        this.evaluacionAlumnoService = evaluacionAlumnoService;
        this.evaluacionAlumnoRepository = evaluacionAlumnoRepository;
    }

    /**
     * {@code POST  /evaluacion-alumnos} : Create a new evaluacionAlumno.
     *
     * @param evaluacionAlumno the evaluacionAlumno to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new evaluacionAlumno, or with status {@code 400 (Bad Request)} if the evaluacionAlumno has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/evaluacion-alumnos")
    public ResponseEntity<EvaluacionAlumno> createEvaluacionAlumno(@RequestBody EvaluacionAlumno evaluacionAlumno)
        throws URISyntaxException {
        log.debug("REST request to save EvaluacionAlumno : {}", evaluacionAlumno);
        if (evaluacionAlumno.getId() != null) {
            throw new BadRequestAlertException("A new evaluacionAlumno cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EvaluacionAlumno result = evaluacionAlumnoService.save(evaluacionAlumno);
        return ResponseEntity
            .created(new URI("/api/evaluacion-alumnos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /evaluacion-alumnos/:id} : Updates an existing evaluacionAlumno.
     *
     * @param id the id of the evaluacionAlumno to save.
     * @param evaluacionAlumno the evaluacionAlumno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evaluacionAlumno,
     * or with status {@code 400 (Bad Request)} if the evaluacionAlumno is not valid,
     * or with status {@code 500 (Internal Server Error)} if the evaluacionAlumno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/evaluacion-alumnos/{id}")
    public ResponseEntity<EvaluacionAlumno> updateEvaluacionAlumno(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EvaluacionAlumno evaluacionAlumno
    ) throws URISyntaxException {
        log.debug("REST request to update EvaluacionAlumno : {}, {}", id, evaluacionAlumno);
        if (evaluacionAlumno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evaluacionAlumno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!evaluacionAlumnoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EvaluacionAlumno result = evaluacionAlumnoService.update(evaluacionAlumno);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, evaluacionAlumno.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /evaluacion-alumnos/:id} : Partial updates given fields of an existing evaluacionAlumno, field will ignore if it is null
     *
     * @param id the id of the evaluacionAlumno to save.
     * @param evaluacionAlumno the evaluacionAlumno to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evaluacionAlumno,
     * or with status {@code 400 (Bad Request)} if the evaluacionAlumno is not valid,
     * or with status {@code 404 (Not Found)} if the evaluacionAlumno is not found,
     * or with status {@code 500 (Internal Server Error)} if the evaluacionAlumno couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/evaluacion-alumnos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EvaluacionAlumno> partialUpdateEvaluacionAlumno(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EvaluacionAlumno evaluacionAlumno
    ) throws URISyntaxException {
        log.debug("REST request to partial update EvaluacionAlumno partially : {}, {}", id, evaluacionAlumno);
        if (evaluacionAlumno.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evaluacionAlumno.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!evaluacionAlumnoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EvaluacionAlumno> result = evaluacionAlumnoService.partialUpdate(evaluacionAlumno);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, evaluacionAlumno.getId().toString())
        );
    }

    /**
     * {@code GET  /evaluacion-alumnos} : get all the evaluacionAlumnos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of evaluacionAlumnos in body.
     */
    @GetMapping("/evaluacion-alumnos")
    public List<EvaluacionAlumno> getAllEvaluacionAlumnos() {
        log.debug("REST request to get all EvaluacionAlumnos");
        return evaluacionAlumnoService.findAll();
    }

    /**
     * {@code GET  /evaluacion-alumnos/:id} : get the "id" evaluacionAlumno.
     *
     * @param id the id of the evaluacionAlumno to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the evaluacionAlumno, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/evaluacion-alumnos/{id}")
    public ResponseEntity<EvaluacionAlumno> getEvaluacionAlumno(@PathVariable Long id) {
        log.debug("REST request to get EvaluacionAlumno : {}", id);
        Optional<EvaluacionAlumno> evaluacionAlumno = evaluacionAlumnoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(evaluacionAlumno);
    }

    /**
     * {@code DELETE  /evaluacion-alumnos/:id} : delete the "id" evaluacionAlumno.
     *
     * @param id the id of the evaluacionAlumno to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/evaluacion-alumnos/{id}")
    public ResponseEntity<Void> deleteEvaluacionAlumno(@PathVariable Long id) {
        log.debug("REST request to delete EvaluacionAlumno : {}", id);
        evaluacionAlumnoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
