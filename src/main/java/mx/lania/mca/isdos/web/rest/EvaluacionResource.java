package mx.lania.mca.isdos.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mx.lania.mca.isdos.domain.Evaluacion;
import mx.lania.mca.isdos.repository.EvaluacionRepository;
import mx.lania.mca.isdos.service.EvaluacionService;
import mx.lania.mca.isdos.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mx.lania.mca.isdos.domain.Evaluacion}.
 */
@RestController
@RequestMapping("/api")
public class EvaluacionResource {

    private final Logger log = LoggerFactory.getLogger(EvaluacionResource.class);

    private static final String ENTITY_NAME = "evaluacion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EvaluacionService evaluacionService;

    private final EvaluacionRepository evaluacionRepository;

    public EvaluacionResource(EvaluacionService evaluacionService, EvaluacionRepository evaluacionRepository) {
        this.evaluacionService = evaluacionService;
        this.evaluacionRepository = evaluacionRepository;
    }

    /**
     * {@code POST  /evaluacions} : Create a new evaluacion.
     *
     * @param evaluacion the evaluacion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new evaluacion, or with status {@code 400 (Bad Request)} if the evaluacion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/evaluacions")
    public ResponseEntity<Evaluacion> createEvaluacion(@Valid @RequestBody Evaluacion evaluacion) throws URISyntaxException {
        log.debug("REST request to save Evaluacion : {}", evaluacion);
        if (evaluacion.getId() != null) {
            throw new BadRequestAlertException("A new evaluacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Evaluacion result = evaluacionService.save(evaluacion);
        return ResponseEntity
            .created(new URI("/api/evaluacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /evaluacions/:id} : Updates an existing evaluacion.
     *
     * @param id the id of the evaluacion to save.
     * @param evaluacion the evaluacion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evaluacion,
     * or with status {@code 400 (Bad Request)} if the evaluacion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the evaluacion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/evaluacions/{id}")
    public ResponseEntity<Evaluacion> updateEvaluacion(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Evaluacion evaluacion
    ) throws URISyntaxException {
        log.debug("REST request to update Evaluacion : {}, {}", id, evaluacion);
        if (evaluacion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evaluacion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!evaluacionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Evaluacion result = evaluacionService.update(evaluacion);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, evaluacion.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /evaluacions/:id} : Partial updates given fields of an existing evaluacion, field will ignore if it is null
     *
     * @param id the id of the evaluacion to save.
     * @param evaluacion the evaluacion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated evaluacion,
     * or with status {@code 400 (Bad Request)} if the evaluacion is not valid,
     * or with status {@code 404 (Not Found)} if the evaluacion is not found,
     * or with status {@code 500 (Internal Server Error)} if the evaluacion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/evaluacions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Evaluacion> partialUpdateEvaluacion(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Evaluacion evaluacion
    ) throws URISyntaxException {
        log.debug("REST request to partial update Evaluacion partially : {}, {}", id, evaluacion);
        if (evaluacion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, evaluacion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!evaluacionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Evaluacion> result = evaluacionService.partialUpdate(evaluacion);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, evaluacion.getId().toString())
        );
    }

    /**
     * {@code GET  /evaluacions} : get all the evaluacions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of evaluacions in body.
     */
    @GetMapping("/evaluacions")
    public List<Evaluacion> getAllEvaluacions() {
        log.debug("REST request to get all Evaluacions");
        return evaluacionService.findAll();
    }

    /**
     * {@code GET  /evaluacions/:id} : get the "id" evaluacion.
     *
     * @param id the id of the evaluacion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the evaluacion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/evaluacions/{id}")
    public ResponseEntity<Evaluacion> getEvaluacion(@PathVariable Long id) {
        log.debug("REST request to get Evaluacion : {}", id);
        Optional<Evaluacion> evaluacion = evaluacionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(evaluacion);
    }

    /**
     * {@code DELETE  /evaluacions/:id} : delete the "id" evaluacion.
     *
     * @param id the id of the evaluacion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/evaluacions/{id}")
    public ResponseEntity<Void> deleteEvaluacion(@PathVariable Long id) {
        log.debug("REST request to delete Evaluacion : {}", id);
        evaluacionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
