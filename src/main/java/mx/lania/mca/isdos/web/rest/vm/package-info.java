/**
 * View Models used by Spring MVC REST controllers.
 */
package mx.lania.mca.isdos.web.rest.vm;
