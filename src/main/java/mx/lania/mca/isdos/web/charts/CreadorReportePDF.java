package mx.lania.mca.isdos.web.charts;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import org.springframework.stereotype.Component;

/**
 *
 * @author Julio
 */
@Component
public class CreadorReportePDF {

    CreadorGraficas cGraficas;

    public CreadorReportePDF(CreadorGraficas cg) {
        this.cGraficas = cg;
    }

    public byte[] crearPDFConDosCharts() throws IOException {
        try ( PDDocument doc = new PDDocument()) {
            PDPage pagina = new PDPage(PDRectangle.LETTER);
            float pageW = pagina.getMediaBox().getWidth();
            float pageH = pagina.getMediaBox().getHeight();

            try ( PDPageContentStream contenido = new PDPageContentStream(doc, pagina)) {
                BufferedImage bImg = cGraficas.graficaLinea(100, "Segundos", "Temperatura", (int) pageW, (int) (pageH / 2));
                PDImageXObject imgGrafica = JPEGFactory.createFromImage(doc, bImg);
                contenido.drawImage(imgGrafica, 0, 0);

                BufferedImage bImg2 = cGraficas.graficaLinea(30, "Dias", "Distancia", (int) pageW, (int) (pageH / 2));
                PDImageXObject imgGrafica2 = JPEGFactory.createFromImage(doc, bImg2);
                contenido.drawImage(imgGrafica2, 0, pageH / 2);
            }

            doc.addPage(pagina);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            doc.save(stream);
            return stream.toByteArray();
        }
    }

    public byte[] crearPDFConCuatroCharts() throws IOException {
        try ( PDDocument doc = new PDDocument()) {
            PDPage pagina = new PDPage(PDRectangle.LETTER);
            pagina.setRotation(90);
            float pageH = pagina.getMediaBox().getWidth();
            float pageW = pagina.getMediaBox().getHeight();

            try ( PDPageContentStream contenido = new PDPageContentStream(doc, pagina)) {
                BufferedImage bImg = cGraficas.graficaLinea(50, "Dias", "Precio", (int) (pageW / 2), (int) (pageH / 2));
                PDImageXObject imgGrafica = JPEGFactory.createFromImage(doc, bImg);
                contenido.transform(new Matrix(0, 1, -1, 0, pageH, 0));
                contenido.drawImage(imgGrafica, 0, 0);
                
                BufferedImage bImg2 = cGraficas.graficaPastel(7, "Departamentos", (int) (pageW / 2), (int) (pageH / 2));
                PDImageXObject imgGrafica2 = JPEGFactory.createFromImage(doc, bImg2);
                contenido.drawImage(imgGrafica2, pageW / 2, 0);

                BufferedImage bImg3 = cGraficas.graficaPastel(9, "Sistemas", (int) (pageW / 2), (int) (pageH / 2));
                PDImageXObject imgGrafica3 = JPEGFactory.createFromImage(doc, bImg3);
                contenido.drawImage(imgGrafica3, 0, pageH /2);

                BufferedImage bImg4 = cGraficas.graficaLinea(30, "Dias", "Distancia", (int) (pageW / 2), (int) (pageH / 2));
                PDImageXObject imgGrafica4 = JPEGFactory.createFromImage(doc, bImg4);
                contenido.drawImage(imgGrafica4, pageW / 2, pageH / 2);
            }

            doc.addPage(pagina);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            doc.save(stream);
            return stream.toByteArray();
        }
    }
}
