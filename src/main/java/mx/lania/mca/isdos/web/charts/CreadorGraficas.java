package mx.lania.mca.isdos.web.charts;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.Styler;
import org.springframework.stereotype.Component;

/**
 *
 * @author Julio
 */
@Component
public class CreadorGraficas {

    BufferedImage graficaLinea(int numPuntos, String tituloX, String tituloY, int w, int h) {
        XYChart chart = new XYChartBuilder()
            .xAxisTitle(tituloX)
            .yAxisTitle(tituloY)
            .width(w)
            .height(h)
            .theme(Styler.ChartTheme.XChart)
            .build();
        chart.addSeries("A", generarLineaRandom(numPuntos));
        chart.addSeries("B", generarLineaRandom(numPuntos));
        return BitmapEncoder.getBufferedImage(chart);
    }

    private double[] generarLineaRandom(int numPuntos) {
        double[] datos = new double[numPuntos];
        datos[0] = 0;
        for (int ii = 1; ii < numPuntos; ii++) {
            datos[ii] = datos[ii - 1] + 0.1 * ThreadLocalRandom.current().nextDouble();
        }
        return datos;
    }
    
    BufferedImage graficaPastel(int numRebanadas, String titulo, int w, int h) {
        PieChart chart = new PieChartBuilder()
                .title(titulo)
                .width(w)
                .height(h)
                .build();
        generarDatos(numRebanadas)
                .forEach((l,v) -> chart.addSeries(l, v));
        return BitmapEncoder.getBufferedImage(chart);
    }

    private Map<String, Number> generarDatos(int numDatos) {
        Map<String, Number> datos = new HashMap<>();
        for (int ii = 0; ii < numDatos; ii++) {
            datos.put(String.valueOf((char)(ii + 'A' - 1)), ThreadLocalRandom.current().nextInt(10, 100));
        }
        return datos;
    }
}
