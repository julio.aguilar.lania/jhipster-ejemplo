package mx.lania.mca.isdos.repository;

import java.util.List;
import mx.lania.mca.isdos.domain.Alumno;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Alumno entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long> {
    @Query(value="SELECT * FROM alumno WHERE CONCAT(nombres,' ',apellidos) LIKE :cadena",
            nativeQuery = true)
    List<Alumno> buscarPorNombre(@Param("cadena") String nombre);
}
