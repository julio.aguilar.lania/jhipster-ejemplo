package mx.lania.mca.isdos.repository;

import mx.lania.mca.isdos.domain.IteracionAlumno;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the IteracionAlumno entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IteracionAlumnoRepository extends JpaRepository<IteracionAlumno, Long> {}
