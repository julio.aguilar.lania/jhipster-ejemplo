package mx.lania.mca.isdos.repository;

import mx.lania.mca.isdos.domain.Evaluacion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Evaluacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvaluacionRepository extends JpaRepository<Evaluacion, Long> {}
