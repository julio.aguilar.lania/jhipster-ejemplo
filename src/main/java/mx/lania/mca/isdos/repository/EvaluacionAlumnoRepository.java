package mx.lania.mca.isdos.repository;

import mx.lania.mca.isdos.domain.EvaluacionAlumno;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EvaluacionAlumno entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvaluacionAlumnoRepository extends JpaRepository<EvaluacionAlumno, Long> {}
