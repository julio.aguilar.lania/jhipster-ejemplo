package mx.lania.mca.isdos.repository;

import mx.lania.mca.isdos.domain.Iteracion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Iteracion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IteracionRepository extends JpaRepository<Iteracion, Long> {}
