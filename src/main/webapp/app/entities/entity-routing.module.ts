import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'equipo',
        data: { pageTitle: 'Equipos' },
        loadChildren: () => import('./equipo/equipo.module').then(m => m.EquipoModule),
      },
      {
        path: 'alumno',
        data: { pageTitle: 'Alumnos' },
        loadChildren: () => import('./alumno/alumno.module').then(m => m.AlumnoModule),
      },
      {
        path: 'iteracion',
        data: { pageTitle: 'Iteracions' },
        loadChildren: () => import('./iteracion/iteracion.module').then(m => m.IteracionModule),
      },
      {
        path: 'iteracion-alumno',
        data: { pageTitle: 'IteracionAlumnos' },
        loadChildren: () => import('./iteracion-alumno/iteracion-alumno.module').then(m => m.IteracionAlumnoModule),
      },
      {
        path: 'evaluacion',
        data: { pageTitle: 'Evaluacions' },
        loadChildren: () => import('./evaluacion/evaluacion.module').then(m => m.EvaluacionModule),
      },
      {
        path: 'evaluacion-alumno',
        data: { pageTitle: 'EvaluacionAlumnos' },
        loadChildren: () => import('./evaluacion-alumno/evaluacion-alumno.module').then(m => m.EvaluacionAlumnoModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
