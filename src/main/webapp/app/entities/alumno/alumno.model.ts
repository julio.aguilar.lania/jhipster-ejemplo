import { IEquipo } from 'app/entities/equipo/equipo.model';

export interface IAlumno {
  id?: number;
  nombres?: string;
  apellidos?: string;
  equipo?: IEquipo | null;
}

export class Alumno implements IAlumno {
  constructor(public id?: number, public nombres?: string, public apellidos?: string, public equipo?: IEquipo | null) {}
}

export function getAlumnoIdentifier(alumno: IAlumno): number | undefined {
  return alumno.id;
}
