import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';

import { EvaluacionAlumnoComponent } from './evaluacion-alumno.component';

describe('EvaluacionAlumno Management Component', () => {
  let comp: EvaluacionAlumnoComponent;
  let fixture: ComponentFixture<EvaluacionAlumnoComponent>;
  let service: EvaluacionAlumnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EvaluacionAlumnoComponent],
    })
      .overrideTemplate(EvaluacionAlumnoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EvaluacionAlumnoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EvaluacionAlumnoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.evaluacionAlumnos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
