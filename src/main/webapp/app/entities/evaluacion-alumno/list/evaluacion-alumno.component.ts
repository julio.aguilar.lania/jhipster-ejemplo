import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEvaluacionAlumno } from '../evaluacion-alumno.model';
import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';
import { EvaluacionAlumnoDeleteDialogComponent } from '../delete/evaluacion-alumno-delete-dialog.component';

@Component({
  selector: 'jhi-evaluacion-alumno',
  templateUrl: './evaluacion-alumno.component.html',
})
export class EvaluacionAlumnoComponent implements OnInit {
  evaluacionAlumnos?: IEvaluacionAlumno[];
  isLoading = false;

  constructor(protected evaluacionAlumnoService: EvaluacionAlumnoService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.evaluacionAlumnoService.query().subscribe({
      next: (res: HttpResponse<IEvaluacionAlumno[]>) => {
        this.isLoading = false;
        this.evaluacionAlumnos = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IEvaluacionAlumno): number {
    return item.id!;
  }

  delete(evaluacionAlumno: IEvaluacionAlumno): void {
    const modalRef = this.modalService.open(EvaluacionAlumnoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.evaluacionAlumno = evaluacionAlumno;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
