import { IEvaluacion } from 'app/entities/evaluacion/evaluacion.model';
import { IAlumno } from 'app/entities/alumno/alumno.model';

export interface IEvaluacionAlumno {
  id?: number;
  resultado?: number | null;
  evaluacion?: IEvaluacion | null;
  alumno?: IAlumno | null;
  evaluador?: IAlumno | null;
}

export class EvaluacionAlumno implements IEvaluacionAlumno {
  constructor(
    public id?: number,
    public resultado?: number | null,
    public evaluacion?: IEvaluacion | null,
    public alumno?: IAlumno | null,
    public evaluador?: IAlumno | null
  ) {}
}

export function getEvaluacionAlumnoIdentifier(evaluacionAlumno: IEvaluacionAlumno): number | undefined {
  return evaluacionAlumno.id;
}
