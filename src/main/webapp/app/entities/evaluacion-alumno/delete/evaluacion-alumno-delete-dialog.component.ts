import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEvaluacionAlumno } from '../evaluacion-alumno.model';
import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';

@Component({
  templateUrl: './evaluacion-alumno-delete-dialog.component.html',
})
export class EvaluacionAlumnoDeleteDialogComponent {
  evaluacionAlumno?: IEvaluacionAlumno;

  constructor(protected evaluacionAlumnoService: EvaluacionAlumnoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.evaluacionAlumnoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
