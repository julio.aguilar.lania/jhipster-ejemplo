import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IEvaluacionAlumno, EvaluacionAlumno } from '../evaluacion-alumno.model';
import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';
import { IEvaluacion } from 'app/entities/evaluacion/evaluacion.model';
import { EvaluacionService } from 'app/entities/evaluacion/service/evaluacion.service';
import { IAlumno } from 'app/entities/alumno/alumno.model';
import { AlumnoService } from 'app/entities/alumno/service/alumno.service';

@Component({
  selector: 'jhi-evaluacion-alumno-update',
  templateUrl: './evaluacion-alumno-update.component.html',
})
export class EvaluacionAlumnoUpdateComponent implements OnInit {
  isSaving = false;

  evaluacionsSharedCollection: IEvaluacion[] = [];
  alumnosSharedCollection: IAlumno[] = [];

  editForm = this.fb.group({
    id: [],
    resultado: [],
    evaluacion: [],
    alumno: [],
    evaluador: [],
  });

  constructor(
    protected evaluacionAlumnoService: EvaluacionAlumnoService,
    protected evaluacionService: EvaluacionService,
    protected alumnoService: AlumnoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ evaluacionAlumno }) => {
      this.updateForm(evaluacionAlumno);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const evaluacionAlumno = this.createFromForm();
    if (evaluacionAlumno.id !== undefined) {
      this.subscribeToSaveResponse(this.evaluacionAlumnoService.update(evaluacionAlumno));
    } else {
      this.subscribeToSaveResponse(this.evaluacionAlumnoService.create(evaluacionAlumno));
    }
  }

  trackEvaluacionById(_index: number, item: IEvaluacion): number {
    return item.id!;
  }

  trackAlumnoById(_index: number, item: IAlumno): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvaluacionAlumno>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(evaluacionAlumno: IEvaluacionAlumno): void {
    this.editForm.patchValue({
      id: evaluacionAlumno.id,
      resultado: evaluacionAlumno.resultado,
      evaluacion: evaluacionAlumno.evaluacion,
      alumno: evaluacionAlumno.alumno,
      evaluador: evaluacionAlumno.evaluador,
    });

    this.evaluacionsSharedCollection = this.evaluacionService.addEvaluacionToCollectionIfMissing(
      this.evaluacionsSharedCollection,
      evaluacionAlumno.evaluacion
    );
    this.alumnosSharedCollection = this.alumnoService.addAlumnoToCollectionIfMissing(
      this.alumnosSharedCollection,
      evaluacionAlumno.alumno,
      evaluacionAlumno.evaluador
    );
  }

  protected loadRelationshipsOptions(): void {
    this.evaluacionService
      .query()
      .pipe(map((res: HttpResponse<IEvaluacion[]>) => res.body ?? []))
      .pipe(
        map((evaluacions: IEvaluacion[]) =>
          this.evaluacionService.addEvaluacionToCollectionIfMissing(evaluacions, this.editForm.get('evaluacion')!.value)
        )
      )
      .subscribe((evaluacions: IEvaluacion[]) => (this.evaluacionsSharedCollection = evaluacions));

    this.alumnoService
      .query()
      .pipe(map((res: HttpResponse<IAlumno[]>) => res.body ?? []))
      .pipe(
        map((alumnos: IAlumno[]) =>
          this.alumnoService.addAlumnoToCollectionIfMissing(
            alumnos,
            this.editForm.get('alumno')!.value,
            this.editForm.get('evaluador')!.value
          )
        )
      )
      .subscribe((alumnos: IAlumno[]) => (this.alumnosSharedCollection = alumnos));
  }

  protected createFromForm(): IEvaluacionAlumno {
    return {
      ...new EvaluacionAlumno(),
      id: this.editForm.get(['id'])!.value,
      resultado: this.editForm.get(['resultado'])!.value,
      evaluacion: this.editForm.get(['evaluacion'])!.value,
      alumno: this.editForm.get(['alumno'])!.value,
      evaluador: this.editForm.get(['evaluador'])!.value,
    };
  }
}
