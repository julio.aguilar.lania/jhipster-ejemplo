import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';
import { IEvaluacionAlumno, EvaluacionAlumno } from '../evaluacion-alumno.model';
import { IEvaluacion } from 'app/entities/evaluacion/evaluacion.model';
import { EvaluacionService } from 'app/entities/evaluacion/service/evaluacion.service';
import { IAlumno } from 'app/entities/alumno/alumno.model';
import { AlumnoService } from 'app/entities/alumno/service/alumno.service';

import { EvaluacionAlumnoUpdateComponent } from './evaluacion-alumno-update.component';

describe('EvaluacionAlumno Management Update Component', () => {
  let comp: EvaluacionAlumnoUpdateComponent;
  let fixture: ComponentFixture<EvaluacionAlumnoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let evaluacionAlumnoService: EvaluacionAlumnoService;
  let evaluacionService: EvaluacionService;
  let alumnoService: AlumnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EvaluacionAlumnoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EvaluacionAlumnoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EvaluacionAlumnoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    evaluacionAlumnoService = TestBed.inject(EvaluacionAlumnoService);
    evaluacionService = TestBed.inject(EvaluacionService);
    alumnoService = TestBed.inject(AlumnoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Evaluacion query and add missing value', () => {
      const evaluacionAlumno: IEvaluacionAlumno = { id: 456 };
      const evaluacion: IEvaluacion = { id: 50476 };
      evaluacionAlumno.evaluacion = evaluacion;

      const evaluacionCollection: IEvaluacion[] = [{ id: 68076 }];
      jest.spyOn(evaluacionService, 'query').mockReturnValue(of(new HttpResponse({ body: evaluacionCollection })));
      const additionalEvaluacions = [evaluacion];
      const expectedCollection: IEvaluacion[] = [...additionalEvaluacions, ...evaluacionCollection];
      jest.spyOn(evaluacionService, 'addEvaluacionToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      expect(evaluacionService.query).toHaveBeenCalled();
      expect(evaluacionService.addEvaluacionToCollectionIfMissing).toHaveBeenCalledWith(evaluacionCollection, ...additionalEvaluacions);
      expect(comp.evaluacionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Alumno query and add missing value', () => {
      const evaluacionAlumno: IEvaluacionAlumno = { id: 456 };
      const alumno: IAlumno = { id: 66324 };
      evaluacionAlumno.alumno = alumno;
      const evaluador: IAlumno = { id: 64422 };
      evaluacionAlumno.evaluador = evaluador;

      const alumnoCollection: IAlumno[] = [{ id: 69389 }];
      jest.spyOn(alumnoService, 'query').mockReturnValue(of(new HttpResponse({ body: alumnoCollection })));
      const additionalAlumnos = [alumno, evaluador];
      const expectedCollection: IAlumno[] = [...additionalAlumnos, ...alumnoCollection];
      jest.spyOn(alumnoService, 'addAlumnoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      expect(alumnoService.query).toHaveBeenCalled();
      expect(alumnoService.addAlumnoToCollectionIfMissing).toHaveBeenCalledWith(alumnoCollection, ...additionalAlumnos);
      expect(comp.alumnosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const evaluacionAlumno: IEvaluacionAlumno = { id: 456 };
      const evaluacion: IEvaluacion = { id: 14223 };
      evaluacionAlumno.evaluacion = evaluacion;
      const alumno: IAlumno = { id: 81231 };
      evaluacionAlumno.alumno = alumno;
      const evaluador: IAlumno = { id: 72215 };
      evaluacionAlumno.evaluador = evaluador;

      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(evaluacionAlumno));
      expect(comp.evaluacionsSharedCollection).toContain(evaluacion);
      expect(comp.alumnosSharedCollection).toContain(alumno);
      expect(comp.alumnosSharedCollection).toContain(evaluador);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EvaluacionAlumno>>();
      const evaluacionAlumno = { id: 123 };
      jest.spyOn(evaluacionAlumnoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: evaluacionAlumno }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(evaluacionAlumnoService.update).toHaveBeenCalledWith(evaluacionAlumno);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EvaluacionAlumno>>();
      const evaluacionAlumno = new EvaluacionAlumno();
      jest.spyOn(evaluacionAlumnoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: evaluacionAlumno }));
      saveSubject.complete();

      // THEN
      expect(evaluacionAlumnoService.create).toHaveBeenCalledWith(evaluacionAlumno);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EvaluacionAlumno>>();
      const evaluacionAlumno = { id: 123 };
      jest.spyOn(evaluacionAlumnoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(evaluacionAlumnoService.update).toHaveBeenCalledWith(evaluacionAlumno);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackEvaluacionById', () => {
      it('Should return tracked Evaluacion primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackEvaluacionById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackAlumnoById', () => {
      it('Should return tracked Alumno primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAlumnoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
