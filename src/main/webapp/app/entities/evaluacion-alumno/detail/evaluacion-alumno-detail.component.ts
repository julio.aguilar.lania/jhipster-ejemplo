import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEvaluacionAlumno } from '../evaluacion-alumno.model';

@Component({
  selector: 'jhi-evaluacion-alumno-detail',
  templateUrl: './evaluacion-alumno-detail.component.html',
})
export class EvaluacionAlumnoDetailComponent implements OnInit {
  evaluacionAlumno: IEvaluacionAlumno | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ evaluacionAlumno }) => {
      this.evaluacionAlumno = evaluacionAlumno;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
