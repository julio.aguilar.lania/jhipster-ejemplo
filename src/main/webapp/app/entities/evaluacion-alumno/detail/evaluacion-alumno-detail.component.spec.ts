import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EvaluacionAlumnoDetailComponent } from './evaluacion-alumno-detail.component';

describe('EvaluacionAlumno Management Detail Component', () => {
  let comp: EvaluacionAlumnoDetailComponent;
  let fixture: ComponentFixture<EvaluacionAlumnoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EvaluacionAlumnoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ evaluacionAlumno: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EvaluacionAlumnoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EvaluacionAlumnoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load evaluacionAlumno on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.evaluacionAlumno).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
