import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EvaluacionAlumnoComponent } from '../list/evaluacion-alumno.component';
import { EvaluacionAlumnoDetailComponent } from '../detail/evaluacion-alumno-detail.component';
import { EvaluacionAlumnoUpdateComponent } from '../update/evaluacion-alumno-update.component';
import { EvaluacionAlumnoRoutingResolveService } from './evaluacion-alumno-routing-resolve.service';

const evaluacionAlumnoRoute: Routes = [
  {
    path: '',
    component: EvaluacionAlumnoComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EvaluacionAlumnoDetailComponent,
    resolve: {
      evaluacionAlumno: EvaluacionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EvaluacionAlumnoUpdateComponent,
    resolve: {
      evaluacionAlumno: EvaluacionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EvaluacionAlumnoUpdateComponent,
    resolve: {
      evaluacionAlumno: EvaluacionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(evaluacionAlumnoRoute)],
  exports: [RouterModule],
})
export class EvaluacionAlumnoRoutingModule {}
