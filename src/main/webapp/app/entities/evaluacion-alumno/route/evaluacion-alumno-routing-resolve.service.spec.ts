import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IEvaluacionAlumno, EvaluacionAlumno } from '../evaluacion-alumno.model';
import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';

import { EvaluacionAlumnoRoutingResolveService } from './evaluacion-alumno-routing-resolve.service';

describe('EvaluacionAlumno routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: EvaluacionAlumnoRoutingResolveService;
  let service: EvaluacionAlumnoService;
  let resultEvaluacionAlumno: IEvaluacionAlumno | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(EvaluacionAlumnoRoutingResolveService);
    service = TestBed.inject(EvaluacionAlumnoService);
    resultEvaluacionAlumno = undefined;
  });

  describe('resolve', () => {
    it('should return IEvaluacionAlumno returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEvaluacionAlumno = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEvaluacionAlumno).toEqual({ id: 123 });
    });

    it('should return new IEvaluacionAlumno if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEvaluacionAlumno = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultEvaluacionAlumno).toEqual(new EvaluacionAlumno());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as EvaluacionAlumno })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEvaluacionAlumno = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEvaluacionAlumno).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
