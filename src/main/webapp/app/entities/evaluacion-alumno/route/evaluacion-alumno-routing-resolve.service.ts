import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEvaluacionAlumno, EvaluacionAlumno } from '../evaluacion-alumno.model';
import { EvaluacionAlumnoService } from '../service/evaluacion-alumno.service';

@Injectable({ providedIn: 'root' })
export class EvaluacionAlumnoRoutingResolveService implements Resolve<IEvaluacionAlumno> {
  constructor(protected service: EvaluacionAlumnoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEvaluacionAlumno> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((evaluacionAlumno: HttpResponse<EvaluacionAlumno>) => {
          if (evaluacionAlumno.body) {
            return of(evaluacionAlumno.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EvaluacionAlumno());
  }
}
