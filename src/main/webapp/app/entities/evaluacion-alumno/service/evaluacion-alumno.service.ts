import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEvaluacionAlumno, getEvaluacionAlumnoIdentifier } from '../evaluacion-alumno.model';

export type EntityResponseType = HttpResponse<IEvaluacionAlumno>;
export type EntityArrayResponseType = HttpResponse<IEvaluacionAlumno[]>;

@Injectable({ providedIn: 'root' })
export class EvaluacionAlumnoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/evaluacion-alumnos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(evaluacionAlumno: IEvaluacionAlumno): Observable<EntityResponseType> {
    return this.http.post<IEvaluacionAlumno>(this.resourceUrl, evaluacionAlumno, { observe: 'response' });
  }

  update(evaluacionAlumno: IEvaluacionAlumno): Observable<EntityResponseType> {
    return this.http.put<IEvaluacionAlumno>(
      `${this.resourceUrl}/${getEvaluacionAlumnoIdentifier(evaluacionAlumno) as number}`,
      evaluacionAlumno,
      { observe: 'response' }
    );
  }

  partialUpdate(evaluacionAlumno: IEvaluacionAlumno): Observable<EntityResponseType> {
    return this.http.patch<IEvaluacionAlumno>(
      `${this.resourceUrl}/${getEvaluacionAlumnoIdentifier(evaluacionAlumno) as number}`,
      evaluacionAlumno,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEvaluacionAlumno>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEvaluacionAlumno[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEvaluacionAlumnoToCollectionIfMissing(
    evaluacionAlumnoCollection: IEvaluacionAlumno[],
    ...evaluacionAlumnosToCheck: (IEvaluacionAlumno | null | undefined)[]
  ): IEvaluacionAlumno[] {
    const evaluacionAlumnos: IEvaluacionAlumno[] = evaluacionAlumnosToCheck.filter(isPresent);
    if (evaluacionAlumnos.length > 0) {
      const evaluacionAlumnoCollectionIdentifiers = evaluacionAlumnoCollection.map(
        evaluacionAlumnoItem => getEvaluacionAlumnoIdentifier(evaluacionAlumnoItem)!
      );
      const evaluacionAlumnosToAdd = evaluacionAlumnos.filter(evaluacionAlumnoItem => {
        const evaluacionAlumnoIdentifier = getEvaluacionAlumnoIdentifier(evaluacionAlumnoItem);
        if (evaluacionAlumnoIdentifier == null || evaluacionAlumnoCollectionIdentifiers.includes(evaluacionAlumnoIdentifier)) {
          return false;
        }
        evaluacionAlumnoCollectionIdentifiers.push(evaluacionAlumnoIdentifier);
        return true;
      });
      return [...evaluacionAlumnosToAdd, ...evaluacionAlumnoCollection];
    }
    return evaluacionAlumnoCollection;
  }
}
