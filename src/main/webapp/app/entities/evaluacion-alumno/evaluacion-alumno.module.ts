import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EvaluacionAlumnoComponent } from './list/evaluacion-alumno.component';
import { EvaluacionAlumnoDetailComponent } from './detail/evaluacion-alumno-detail.component';
import { EvaluacionAlumnoUpdateComponent } from './update/evaluacion-alumno-update.component';
import { EvaluacionAlumnoDeleteDialogComponent } from './delete/evaluacion-alumno-delete-dialog.component';
import { EvaluacionAlumnoRoutingModule } from './route/evaluacion-alumno-routing.module';

@NgModule({
  imports: [SharedModule, EvaluacionAlumnoRoutingModule],
  declarations: [
    EvaluacionAlumnoComponent,
    EvaluacionAlumnoDetailComponent,
    EvaluacionAlumnoUpdateComponent,
    EvaluacionAlumnoDeleteDialogComponent,
  ],
  entryComponents: [EvaluacionAlumnoDeleteDialogComponent],
})
export class EvaluacionAlumnoModule {}
