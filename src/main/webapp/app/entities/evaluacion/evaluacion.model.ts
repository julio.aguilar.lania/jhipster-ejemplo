import dayjs from 'dayjs/esm';

export interface IEvaluacion {
  id?: number;
  fecha?: dayjs.Dayjs;
}

export class Evaluacion implements IEvaluacion {
  constructor(public id?: number, public fecha?: dayjs.Dayjs) {}
}

export function getEvaluacionIdentifier(evaluacion: IEvaluacion): number | undefined {
  return evaluacion.id;
}
