import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EvaluacionComponent } from '../list/evaluacion.component';
import { EvaluacionDetailComponent } from '../detail/evaluacion-detail.component';
import { EvaluacionUpdateComponent } from '../update/evaluacion-update.component';
import { EvaluacionRoutingResolveService } from './evaluacion-routing-resolve.service';

const evaluacionRoute: Routes = [
  {
    path: '',
    component: EvaluacionComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EvaluacionDetailComponent,
    resolve: {
      evaluacion: EvaluacionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EvaluacionUpdateComponent,
    resolve: {
      evaluacion: EvaluacionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EvaluacionUpdateComponent,
    resolve: {
      evaluacion: EvaluacionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(evaluacionRoute)],
  exports: [RouterModule],
})
export class EvaluacionRoutingModule {}
