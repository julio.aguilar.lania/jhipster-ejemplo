import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEvaluacion, Evaluacion } from '../evaluacion.model';
import { EvaluacionService } from '../service/evaluacion.service';

@Injectable({ providedIn: 'root' })
export class EvaluacionRoutingResolveService implements Resolve<IEvaluacion> {
  constructor(protected service: EvaluacionService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEvaluacion> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((evaluacion: HttpResponse<Evaluacion>) => {
          if (evaluacion.body) {
            return of(evaluacion.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Evaluacion());
  }
}
