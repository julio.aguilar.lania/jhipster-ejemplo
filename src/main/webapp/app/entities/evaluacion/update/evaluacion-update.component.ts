import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IEvaluacion, Evaluacion } from '../evaluacion.model';
import { EvaluacionService } from '../service/evaluacion.service';

@Component({
  selector: 'jhi-evaluacion-update',
  templateUrl: './evaluacion-update.component.html',
})
export class EvaluacionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    fecha: [null, [Validators.required]],
  });

  constructor(protected evaluacionService: EvaluacionService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ evaluacion }) => {
      this.updateForm(evaluacion);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const evaluacion = this.createFromForm();
    if (evaluacion.id !== undefined) {
      this.subscribeToSaveResponse(this.evaluacionService.update(evaluacion));
    } else {
      this.subscribeToSaveResponse(this.evaluacionService.create(evaluacion));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEvaluacion>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(evaluacion: IEvaluacion): void {
    this.editForm.patchValue({
      id: evaluacion.id,
      fecha: evaluacion.fecha,
    });
  }

  protected createFromForm(): IEvaluacion {
    return {
      ...new Evaluacion(),
      id: this.editForm.get(['id'])!.value,
      fecha: this.editForm.get(['fecha'])!.value,
    };
  }
}
