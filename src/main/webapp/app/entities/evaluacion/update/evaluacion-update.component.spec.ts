import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EvaluacionService } from '../service/evaluacion.service';
import { IEvaluacion, Evaluacion } from '../evaluacion.model';

import { EvaluacionUpdateComponent } from './evaluacion-update.component';

describe('Evaluacion Management Update Component', () => {
  let comp: EvaluacionUpdateComponent;
  let fixture: ComponentFixture<EvaluacionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let evaluacionService: EvaluacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EvaluacionUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EvaluacionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EvaluacionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    evaluacionService = TestBed.inject(EvaluacionService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const evaluacion: IEvaluacion = { id: 456 };

      activatedRoute.data = of({ evaluacion });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(evaluacion));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Evaluacion>>();
      const evaluacion = { id: 123 };
      jest.spyOn(evaluacionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: evaluacion }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(evaluacionService.update).toHaveBeenCalledWith(evaluacion);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Evaluacion>>();
      const evaluacion = new Evaluacion();
      jest.spyOn(evaluacionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: evaluacion }));
      saveSubject.complete();

      // THEN
      expect(evaluacionService.create).toHaveBeenCalledWith(evaluacion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Evaluacion>>();
      const evaluacion = { id: 123 };
      jest.spyOn(evaluacionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ evaluacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(evaluacionService.update).toHaveBeenCalledWith(evaluacion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
