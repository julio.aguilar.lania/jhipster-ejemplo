import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEvaluacion } from '../evaluacion.model';
import { EvaluacionService } from '../service/evaluacion.service';

@Component({
  templateUrl: './evaluacion-delete-dialog.component.html',
})
export class EvaluacionDeleteDialogComponent {
  evaluacion?: IEvaluacion;

  constructor(protected evaluacionService: EvaluacionService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.evaluacionService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
