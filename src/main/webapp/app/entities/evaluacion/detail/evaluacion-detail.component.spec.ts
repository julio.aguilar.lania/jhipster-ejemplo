import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EvaluacionDetailComponent } from './evaluacion-detail.component';

describe('Evaluacion Management Detail Component', () => {
  let comp: EvaluacionDetailComponent;
  let fixture: ComponentFixture<EvaluacionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EvaluacionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ evaluacion: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EvaluacionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EvaluacionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load evaluacion on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.evaluacion).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
