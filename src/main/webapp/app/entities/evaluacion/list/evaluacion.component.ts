import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEvaluacion } from '../evaluacion.model';
import { EvaluacionService } from '../service/evaluacion.service';
import { EvaluacionDeleteDialogComponent } from '../delete/evaluacion-delete-dialog.component';

@Component({
  selector: 'jhi-evaluacion',
  templateUrl: './evaluacion.component.html',
})
export class EvaluacionComponent implements OnInit {
  evaluacions?: IEvaluacion[];
  isLoading = false;

  constructor(protected evaluacionService: EvaluacionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.evaluacionService.query().subscribe({
      next: (res: HttpResponse<IEvaluacion[]>) => {
        this.isLoading = false;
        this.evaluacions = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IEvaluacion): number {
    return item.id!;
  }

  delete(evaluacion: IEvaluacion): void {
    const modalRef = this.modalService.open(EvaluacionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.evaluacion = evaluacion;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
