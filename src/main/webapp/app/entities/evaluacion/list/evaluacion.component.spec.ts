import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EvaluacionService } from '../service/evaluacion.service';

import { EvaluacionComponent } from './evaluacion.component';

describe('Evaluacion Management Component', () => {
  let comp: EvaluacionComponent;
  let fixture: ComponentFixture<EvaluacionComponent>;
  let service: EvaluacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EvaluacionComponent],
    })
      .overrideTemplate(EvaluacionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EvaluacionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EvaluacionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.evaluacions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
