import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EvaluacionComponent } from './list/evaluacion.component';
import { EvaluacionDetailComponent } from './detail/evaluacion-detail.component';
import { EvaluacionUpdateComponent } from './update/evaluacion-update.component';
import { EvaluacionDeleteDialogComponent } from './delete/evaluacion-delete-dialog.component';
import { EvaluacionRoutingModule } from './route/evaluacion-routing.module';

@NgModule({
  imports: [SharedModule, EvaluacionRoutingModule],
  declarations: [EvaluacionComponent, EvaluacionDetailComponent, EvaluacionUpdateComponent, EvaluacionDeleteDialogComponent],
  entryComponents: [EvaluacionDeleteDialogComponent],
})
export class EvaluacionModule {}
