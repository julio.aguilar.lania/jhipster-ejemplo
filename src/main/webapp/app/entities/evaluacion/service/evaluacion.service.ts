import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEvaluacion, getEvaluacionIdentifier } from '../evaluacion.model';

export type EntityResponseType = HttpResponse<IEvaluacion>;
export type EntityArrayResponseType = HttpResponse<IEvaluacion[]>;

@Injectable({ providedIn: 'root' })
export class EvaluacionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/evaluacions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(evaluacion: IEvaluacion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(evaluacion);
    return this.http
      .post<IEvaluacion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(evaluacion: IEvaluacion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(evaluacion);
    return this.http
      .put<IEvaluacion>(`${this.resourceUrl}/${getEvaluacionIdentifier(evaluacion) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(evaluacion: IEvaluacion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(evaluacion);
    return this.http
      .patch<IEvaluacion>(`${this.resourceUrl}/${getEvaluacionIdentifier(evaluacion) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEvaluacion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEvaluacion[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEvaluacionToCollectionIfMissing(
    evaluacionCollection: IEvaluacion[],
    ...evaluacionsToCheck: (IEvaluacion | null | undefined)[]
  ): IEvaluacion[] {
    const evaluacions: IEvaluacion[] = evaluacionsToCheck.filter(isPresent);
    if (evaluacions.length > 0) {
      const evaluacionCollectionIdentifiers = evaluacionCollection.map(evaluacionItem => getEvaluacionIdentifier(evaluacionItem)!);
      const evaluacionsToAdd = evaluacions.filter(evaluacionItem => {
        const evaluacionIdentifier = getEvaluacionIdentifier(evaluacionItem);
        if (evaluacionIdentifier == null || evaluacionCollectionIdentifiers.includes(evaluacionIdentifier)) {
          return false;
        }
        evaluacionCollectionIdentifiers.push(evaluacionIdentifier);
        return true;
      });
      return [...evaluacionsToAdd, ...evaluacionCollection];
    }
    return evaluacionCollection;
  }

  protected convertDateFromClient(evaluacion: IEvaluacion): IEvaluacion {
    return Object.assign({}, evaluacion, {
      fecha: evaluacion.fecha?.isValid() ? evaluacion.fecha.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fecha = res.body.fecha ? dayjs(res.body.fecha) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((evaluacion: IEvaluacion) => {
        evaluacion.fecha = evaluacion.fecha ? dayjs(evaluacion.fecha) : undefined;
      });
    }
    return res;
  }
}
