import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IEvaluacion, Evaluacion } from '../evaluacion.model';

import { EvaluacionService } from './evaluacion.service';

describe('Evaluacion Service', () => {
  let service: EvaluacionService;
  let httpMock: HttpTestingController;
  let elemDefault: IEvaluacion;
  let expectedResult: IEvaluacion | IEvaluacion[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EvaluacionService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      fecha: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          fecha: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Evaluacion', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          fecha: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fecha: currentDate,
        },
        returnedFromService
      );

      service.create(new Evaluacion()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Evaluacion', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          fecha: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fecha: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Evaluacion', () => {
      const patchObject = Object.assign({}, new Evaluacion());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          fecha: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Evaluacion', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          fecha: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fecha: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Evaluacion', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEvaluacionToCollectionIfMissing', () => {
      it('should add a Evaluacion to an empty array', () => {
        const evaluacion: IEvaluacion = { id: 123 };
        expectedResult = service.addEvaluacionToCollectionIfMissing([], evaluacion);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(evaluacion);
      });

      it('should not add a Evaluacion to an array that contains it', () => {
        const evaluacion: IEvaluacion = { id: 123 };
        const evaluacionCollection: IEvaluacion[] = [
          {
            ...evaluacion,
          },
          { id: 456 },
        ];
        expectedResult = service.addEvaluacionToCollectionIfMissing(evaluacionCollection, evaluacion);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Evaluacion to an array that doesn't contain it", () => {
        const evaluacion: IEvaluacion = { id: 123 };
        const evaluacionCollection: IEvaluacion[] = [{ id: 456 }];
        expectedResult = service.addEvaluacionToCollectionIfMissing(evaluacionCollection, evaluacion);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(evaluacion);
      });

      it('should add only unique Evaluacion to an array', () => {
        const evaluacionArray: IEvaluacion[] = [{ id: 123 }, { id: 456 }, { id: 26079 }];
        const evaluacionCollection: IEvaluacion[] = [{ id: 123 }];
        expectedResult = service.addEvaluacionToCollectionIfMissing(evaluacionCollection, ...evaluacionArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const evaluacion: IEvaluacion = { id: 123 };
        const evaluacion2: IEvaluacion = { id: 456 };
        expectedResult = service.addEvaluacionToCollectionIfMissing([], evaluacion, evaluacion2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(evaluacion);
        expect(expectedResult).toContain(evaluacion2);
      });

      it('should accept null and undefined values', () => {
        const evaluacion: IEvaluacion = { id: 123 };
        expectedResult = service.addEvaluacionToCollectionIfMissing([], null, evaluacion, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(evaluacion);
      });

      it('should return initial array if no Evaluacion is added', () => {
        const evaluacionCollection: IEvaluacion[] = [{ id: 123 }];
        expectedResult = service.addEvaluacionToCollectionIfMissing(evaluacionCollection, undefined, null);
        expect(expectedResult).toEqual(evaluacionCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
