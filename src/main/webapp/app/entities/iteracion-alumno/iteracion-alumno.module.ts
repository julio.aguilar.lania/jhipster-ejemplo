import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { IteracionAlumnoComponent } from './list/iteracion-alumno.component';
import { IteracionAlumnoDetailComponent } from './detail/iteracion-alumno-detail.component';
import { IteracionAlumnoUpdateComponent } from './update/iteracion-alumno-update.component';
import { IteracionAlumnoDeleteDialogComponent } from './delete/iteracion-alumno-delete-dialog.component';
import { IteracionAlumnoRoutingModule } from './route/iteracion-alumno-routing.module';

@NgModule({
  imports: [SharedModule, IteracionAlumnoRoutingModule],
  declarations: [
    IteracionAlumnoComponent,
    IteracionAlumnoDetailComponent,
    IteracionAlumnoUpdateComponent,
    IteracionAlumnoDeleteDialogComponent,
  ],
  entryComponents: [IteracionAlumnoDeleteDialogComponent],
})
export class IteracionAlumnoModule {}
