import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IIteracionAlumno, IteracionAlumno } from '../iteracion-alumno.model';

import { IteracionAlumnoService } from './iteracion-alumno.service';

describe('IteracionAlumno Service', () => {
  let service: IteracionAlumnoService;
  let httpMock: HttpTestingController;
  let elemDefault: IIteracionAlumno;
  let expectedResult: IIteracionAlumno | IIteracionAlumno[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(IteracionAlumnoService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      actividad: 'AAAAAAA',
      terminado: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a IteracionAlumno', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new IteracionAlumno()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a IteracionAlumno', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          actividad: 'BBBBBB',
          terminado: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a IteracionAlumno', () => {
      const patchObject = Object.assign(
        {
          actividad: 'BBBBBB',
        },
        new IteracionAlumno()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of IteracionAlumno', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          actividad: 'BBBBBB',
          terminado: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a IteracionAlumno', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addIteracionAlumnoToCollectionIfMissing', () => {
      it('should add a IteracionAlumno to an empty array', () => {
        const iteracionAlumno: IIteracionAlumno = { id: 123 };
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing([], iteracionAlumno);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iteracionAlumno);
      });

      it('should not add a IteracionAlumno to an array that contains it', () => {
        const iteracionAlumno: IIteracionAlumno = { id: 123 };
        const iteracionAlumnoCollection: IIteracionAlumno[] = [
          {
            ...iteracionAlumno,
          },
          { id: 456 },
        ];
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing(iteracionAlumnoCollection, iteracionAlumno);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a IteracionAlumno to an array that doesn't contain it", () => {
        const iteracionAlumno: IIteracionAlumno = { id: 123 };
        const iteracionAlumnoCollection: IIteracionAlumno[] = [{ id: 456 }];
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing(iteracionAlumnoCollection, iteracionAlumno);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iteracionAlumno);
      });

      it('should add only unique IteracionAlumno to an array', () => {
        const iteracionAlumnoArray: IIteracionAlumno[] = [{ id: 123 }, { id: 456 }, { id: 90067 }];
        const iteracionAlumnoCollection: IIteracionAlumno[] = [{ id: 123 }];
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing(iteracionAlumnoCollection, ...iteracionAlumnoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const iteracionAlumno: IIteracionAlumno = { id: 123 };
        const iteracionAlumno2: IIteracionAlumno = { id: 456 };
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing([], iteracionAlumno, iteracionAlumno2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iteracionAlumno);
        expect(expectedResult).toContain(iteracionAlumno2);
      });

      it('should accept null and undefined values', () => {
        const iteracionAlumno: IIteracionAlumno = { id: 123 };
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing([], null, iteracionAlumno, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iteracionAlumno);
      });

      it('should return initial array if no IteracionAlumno is added', () => {
        const iteracionAlumnoCollection: IIteracionAlumno[] = [{ id: 123 }];
        expectedResult = service.addIteracionAlumnoToCollectionIfMissing(iteracionAlumnoCollection, undefined, null);
        expect(expectedResult).toEqual(iteracionAlumnoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
