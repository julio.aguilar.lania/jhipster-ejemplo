import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IIteracionAlumno, getIteracionAlumnoIdentifier } from '../iteracion-alumno.model';

export type EntityResponseType = HttpResponse<IIteracionAlumno>;
export type EntityArrayResponseType = HttpResponse<IIteracionAlumno[]>;

@Injectable({ providedIn: 'root' })
export class IteracionAlumnoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/iteracion-alumnos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(iteracionAlumno: IIteracionAlumno): Observable<EntityResponseType> {
    return this.http.post<IIteracionAlumno>(this.resourceUrl, iteracionAlumno, { observe: 'response' });
  }

  update(iteracionAlumno: IIteracionAlumno): Observable<EntityResponseType> {
    return this.http.put<IIteracionAlumno>(
      `${this.resourceUrl}/${getIteracionAlumnoIdentifier(iteracionAlumno) as number}`,
      iteracionAlumno,
      { observe: 'response' }
    );
  }

  partialUpdate(iteracionAlumno: IIteracionAlumno): Observable<EntityResponseType> {
    return this.http.patch<IIteracionAlumno>(
      `${this.resourceUrl}/${getIteracionAlumnoIdentifier(iteracionAlumno) as number}`,
      iteracionAlumno,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IIteracionAlumno>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IIteracionAlumno[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addIteracionAlumnoToCollectionIfMissing(
    iteracionAlumnoCollection: IIteracionAlumno[],
    ...iteracionAlumnosToCheck: (IIteracionAlumno | null | undefined)[]
  ): IIteracionAlumno[] {
    const iteracionAlumnos: IIteracionAlumno[] = iteracionAlumnosToCheck.filter(isPresent);
    if (iteracionAlumnos.length > 0) {
      const iteracionAlumnoCollectionIdentifiers = iteracionAlumnoCollection.map(
        iteracionAlumnoItem => getIteracionAlumnoIdentifier(iteracionAlumnoItem)!
      );
      const iteracionAlumnosToAdd = iteracionAlumnos.filter(iteracionAlumnoItem => {
        const iteracionAlumnoIdentifier = getIteracionAlumnoIdentifier(iteracionAlumnoItem);
        if (iteracionAlumnoIdentifier == null || iteracionAlumnoCollectionIdentifiers.includes(iteracionAlumnoIdentifier)) {
          return false;
        }
        iteracionAlumnoCollectionIdentifiers.push(iteracionAlumnoIdentifier);
        return true;
      });
      return [...iteracionAlumnosToAdd, ...iteracionAlumnoCollection];
    }
    return iteracionAlumnoCollection;
  }
}
