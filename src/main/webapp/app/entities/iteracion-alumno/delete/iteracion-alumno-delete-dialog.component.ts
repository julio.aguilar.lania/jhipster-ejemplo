import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IIteracionAlumno } from '../iteracion-alumno.model';
import { IteracionAlumnoService } from '../service/iteracion-alumno.service';

@Component({
  templateUrl: './iteracion-alumno-delete-dialog.component.html',
})
export class IteracionAlumnoDeleteDialogComponent {
  iteracionAlumno?: IIteracionAlumno;

  constructor(protected iteracionAlumnoService: IteracionAlumnoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.iteracionAlumnoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
