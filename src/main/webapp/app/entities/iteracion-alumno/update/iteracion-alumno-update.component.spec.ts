import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IteracionAlumnoService } from '../service/iteracion-alumno.service';
import { IIteracionAlumno, IteracionAlumno } from '../iteracion-alumno.model';
import { IIteracion } from 'app/entities/iteracion/iteracion.model';
import { IteracionService } from 'app/entities/iteracion/service/iteracion.service';
import { IAlumno } from 'app/entities/alumno/alumno.model';
import { AlumnoService } from 'app/entities/alumno/service/alumno.service';

import { IteracionAlumnoUpdateComponent } from './iteracion-alumno-update.component';

describe('IteracionAlumno Management Update Component', () => {
  let comp: IteracionAlumnoUpdateComponent;
  let fixture: ComponentFixture<IteracionAlumnoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let iteracionAlumnoService: IteracionAlumnoService;
  let iteracionService: IteracionService;
  let alumnoService: AlumnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [IteracionAlumnoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(IteracionAlumnoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IteracionAlumnoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    iteracionAlumnoService = TestBed.inject(IteracionAlumnoService);
    iteracionService = TestBed.inject(IteracionService);
    alumnoService = TestBed.inject(AlumnoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Iteracion query and add missing value', () => {
      const iteracionAlumno: IIteracionAlumno = { id: 456 };
      const iteracion: IIteracion = { id: 35925 };
      iteracionAlumno.iteracion = iteracion;

      const iteracionCollection: IIteracion[] = [{ id: 18899 }];
      jest.spyOn(iteracionService, 'query').mockReturnValue(of(new HttpResponse({ body: iteracionCollection })));
      const additionalIteracions = [iteracion];
      const expectedCollection: IIteracion[] = [...additionalIteracions, ...iteracionCollection];
      jest.spyOn(iteracionService, 'addIteracionToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      expect(iteracionService.query).toHaveBeenCalled();
      expect(iteracionService.addIteracionToCollectionIfMissing).toHaveBeenCalledWith(iteracionCollection, ...additionalIteracions);
      expect(comp.iteracionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Alumno query and add missing value', () => {
      const iteracionAlumno: IIteracionAlumno = { id: 456 };
      const alumno: IAlumno = { id: 49123 };
      iteracionAlumno.alumno = alumno;

      const alumnoCollection: IAlumno[] = [{ id: 54268 }];
      jest.spyOn(alumnoService, 'query').mockReturnValue(of(new HttpResponse({ body: alumnoCollection })));
      const additionalAlumnos = [alumno];
      const expectedCollection: IAlumno[] = [...additionalAlumnos, ...alumnoCollection];
      jest.spyOn(alumnoService, 'addAlumnoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      expect(alumnoService.query).toHaveBeenCalled();
      expect(alumnoService.addAlumnoToCollectionIfMissing).toHaveBeenCalledWith(alumnoCollection, ...additionalAlumnos);
      expect(comp.alumnosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const iteracionAlumno: IIteracionAlumno = { id: 456 };
      const iteracion: IIteracion = { id: 91028 };
      iteracionAlumno.iteracion = iteracion;
      const alumno: IAlumno = { id: 29147 };
      iteracionAlumno.alumno = alumno;

      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(iteracionAlumno));
      expect(comp.iteracionsSharedCollection).toContain(iteracion);
      expect(comp.alumnosSharedCollection).toContain(alumno);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IteracionAlumno>>();
      const iteracionAlumno = { id: 123 };
      jest.spyOn(iteracionAlumnoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iteracionAlumno }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(iteracionAlumnoService.update).toHaveBeenCalledWith(iteracionAlumno);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IteracionAlumno>>();
      const iteracionAlumno = new IteracionAlumno();
      jest.spyOn(iteracionAlumnoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iteracionAlumno }));
      saveSubject.complete();

      // THEN
      expect(iteracionAlumnoService.create).toHaveBeenCalledWith(iteracionAlumno);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IteracionAlumno>>();
      const iteracionAlumno = { id: 123 };
      jest.spyOn(iteracionAlumnoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracionAlumno });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(iteracionAlumnoService.update).toHaveBeenCalledWith(iteracionAlumno);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackIteracionById', () => {
      it('Should return tracked Iteracion primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIteracionById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackAlumnoById', () => {
      it('Should return tracked Alumno primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAlumnoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
