import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IIteracionAlumno, IteracionAlumno } from '../iteracion-alumno.model';
import { IteracionAlumnoService } from '../service/iteracion-alumno.service';
import { IIteracion } from 'app/entities/iteracion/iteracion.model';
import { IteracionService } from 'app/entities/iteracion/service/iteracion.service';
import { IAlumno } from 'app/entities/alumno/alumno.model';
import { AlumnoService } from 'app/entities/alumno/service/alumno.service';

@Component({
  selector: 'jhi-iteracion-alumno-update',
  templateUrl: './iteracion-alumno-update.component.html',
})
export class IteracionAlumnoUpdateComponent implements OnInit {
  isSaving = false;

  iteracionsSharedCollection: IIteracion[] = [];
  alumnosSharedCollection: IAlumno[] = [];

  editForm = this.fb.group({
    id: [],
    actividad: [],
    terminado: [],
    iteracion: [],
    alumno: [],
  });

  constructor(
    protected iteracionAlumnoService: IteracionAlumnoService,
    protected iteracionService: IteracionService,
    protected alumnoService: AlumnoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ iteracionAlumno }) => {
      this.updateForm(iteracionAlumno);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const iteracionAlumno = this.createFromForm();
    if (iteracionAlumno.id !== undefined) {
      this.subscribeToSaveResponse(this.iteracionAlumnoService.update(iteracionAlumno));
    } else {
      this.subscribeToSaveResponse(this.iteracionAlumnoService.create(iteracionAlumno));
    }
  }

  trackIteracionById(_index: number, item: IIteracion): number {
    return item.id!;
  }

  trackAlumnoById(_index: number, item: IAlumno): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIteracionAlumno>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(iteracionAlumno: IIteracionAlumno): void {
    this.editForm.patchValue({
      id: iteracionAlumno.id,
      actividad: iteracionAlumno.actividad,
      terminado: iteracionAlumno.terminado,
      iteracion: iteracionAlumno.iteracion,
      alumno: iteracionAlumno.alumno,
    });

    this.iteracionsSharedCollection = this.iteracionService.addIteracionToCollectionIfMissing(
      this.iteracionsSharedCollection,
      iteracionAlumno.iteracion
    );
    this.alumnosSharedCollection = this.alumnoService.addAlumnoToCollectionIfMissing(this.alumnosSharedCollection, iteracionAlumno.alumno);
  }

  protected loadRelationshipsOptions(): void {
    this.iteracionService
      .query()
      .pipe(map((res: HttpResponse<IIteracion[]>) => res.body ?? []))
      .pipe(
        map((iteracions: IIteracion[]) =>
          this.iteracionService.addIteracionToCollectionIfMissing(iteracions, this.editForm.get('iteracion')!.value)
        )
      )
      .subscribe((iteracions: IIteracion[]) => (this.iteracionsSharedCollection = iteracions));

    this.alumnoService
      .query()
      .pipe(map((res: HttpResponse<IAlumno[]>) => res.body ?? []))
      .pipe(map((alumnos: IAlumno[]) => this.alumnoService.addAlumnoToCollectionIfMissing(alumnos, this.editForm.get('alumno')!.value)))
      .subscribe((alumnos: IAlumno[]) => (this.alumnosSharedCollection = alumnos));
  }

  protected createFromForm(): IIteracionAlumno {
    return {
      ...new IteracionAlumno(),
      id: this.editForm.get(['id'])!.value,
      actividad: this.editForm.get(['actividad'])!.value,
      terminado: this.editForm.get(['terminado'])!.value,
      iteracion: this.editForm.get(['iteracion'])!.value,
      alumno: this.editForm.get(['alumno'])!.value,
    };
  }
}
