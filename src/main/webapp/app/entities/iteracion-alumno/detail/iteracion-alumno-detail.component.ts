import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIteracionAlumno } from '../iteracion-alumno.model';

@Component({
  selector: 'jhi-iteracion-alumno-detail',
  templateUrl: './iteracion-alumno-detail.component.html',
})
export class IteracionAlumnoDetailComponent implements OnInit {
  iteracionAlumno: IIteracionAlumno | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ iteracionAlumno }) => {
      this.iteracionAlumno = iteracionAlumno;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
