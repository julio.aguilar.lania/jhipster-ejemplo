import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IteracionAlumnoDetailComponent } from './iteracion-alumno-detail.component';

describe('IteracionAlumno Management Detail Component', () => {
  let comp: IteracionAlumnoDetailComponent;
  let fixture: ComponentFixture<IteracionAlumnoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IteracionAlumnoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ iteracionAlumno: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(IteracionAlumnoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(IteracionAlumnoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load iteracionAlumno on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.iteracionAlumno).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
