import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IIteracionAlumno } from '../iteracion-alumno.model';
import { IteracionAlumnoService } from '../service/iteracion-alumno.service';
import { IteracionAlumnoDeleteDialogComponent } from '../delete/iteracion-alumno-delete-dialog.component';

@Component({
  selector: 'jhi-iteracion-alumno',
  templateUrl: './iteracion-alumno.component.html',
})
export class IteracionAlumnoComponent implements OnInit {
  iteracionAlumnos?: IIteracionAlumno[];
  isLoading = false;

  constructor(protected iteracionAlumnoService: IteracionAlumnoService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.iteracionAlumnoService.query().subscribe({
      next: (res: HttpResponse<IIteracionAlumno[]>) => {
        this.isLoading = false;
        this.iteracionAlumnos = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IIteracionAlumno): number {
    return item.id!;
  }

  delete(iteracionAlumno: IIteracionAlumno): void {
    const modalRef = this.modalService.open(IteracionAlumnoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.iteracionAlumno = iteracionAlumno;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
