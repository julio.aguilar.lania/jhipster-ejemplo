import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { IteracionAlumnoService } from '../service/iteracion-alumno.service';

import { IteracionAlumnoComponent } from './iteracion-alumno.component';

describe('IteracionAlumno Management Component', () => {
  let comp: IteracionAlumnoComponent;
  let fixture: ComponentFixture<IteracionAlumnoComponent>;
  let service: IteracionAlumnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [IteracionAlumnoComponent],
    })
      .overrideTemplate(IteracionAlumnoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IteracionAlumnoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(IteracionAlumnoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.iteracionAlumnos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
