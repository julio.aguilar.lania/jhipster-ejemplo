import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { IteracionAlumnoComponent } from '../list/iteracion-alumno.component';
import { IteracionAlumnoDetailComponent } from '../detail/iteracion-alumno-detail.component';
import { IteracionAlumnoUpdateComponent } from '../update/iteracion-alumno-update.component';
import { IteracionAlumnoRoutingResolveService } from './iteracion-alumno-routing-resolve.service';

const iteracionAlumnoRoute: Routes = [
  {
    path: '',
    component: IteracionAlumnoComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: IteracionAlumnoDetailComponent,
    resolve: {
      iteracionAlumno: IteracionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: IteracionAlumnoUpdateComponent,
    resolve: {
      iteracionAlumno: IteracionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: IteracionAlumnoUpdateComponent,
    resolve: {
      iteracionAlumno: IteracionAlumnoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(iteracionAlumnoRoute)],
  exports: [RouterModule],
})
export class IteracionAlumnoRoutingModule {}
