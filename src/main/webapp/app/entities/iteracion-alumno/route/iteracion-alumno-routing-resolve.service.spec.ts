import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IIteracionAlumno, IteracionAlumno } from '../iteracion-alumno.model';
import { IteracionAlumnoService } from '../service/iteracion-alumno.service';

import { IteracionAlumnoRoutingResolveService } from './iteracion-alumno-routing-resolve.service';

describe('IteracionAlumno routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: IteracionAlumnoRoutingResolveService;
  let service: IteracionAlumnoService;
  let resultIteracionAlumno: IIteracionAlumno | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(IteracionAlumnoRoutingResolveService);
    service = TestBed.inject(IteracionAlumnoService);
    resultIteracionAlumno = undefined;
  });

  describe('resolve', () => {
    it('should return IIteracionAlumno returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultIteracionAlumno = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultIteracionAlumno).toEqual({ id: 123 });
    });

    it('should return new IIteracionAlumno if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultIteracionAlumno = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultIteracionAlumno).toEqual(new IteracionAlumno());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as IteracionAlumno })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultIteracionAlumno = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultIteracionAlumno).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
