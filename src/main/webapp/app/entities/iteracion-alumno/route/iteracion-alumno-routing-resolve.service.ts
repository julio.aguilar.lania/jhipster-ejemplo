import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IIteracionAlumno, IteracionAlumno } from '../iteracion-alumno.model';
import { IteracionAlumnoService } from '../service/iteracion-alumno.service';

@Injectable({ providedIn: 'root' })
export class IteracionAlumnoRoutingResolveService implements Resolve<IIteracionAlumno> {
  constructor(protected service: IteracionAlumnoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIteracionAlumno> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((iteracionAlumno: HttpResponse<IteracionAlumno>) => {
          if (iteracionAlumno.body) {
            return of(iteracionAlumno.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new IteracionAlumno());
  }
}
