import { IIteracion } from 'app/entities/iteracion/iteracion.model';
import { IAlumno } from 'app/entities/alumno/alumno.model';

export interface IIteracionAlumno {
  id?: number;
  actividad?: string | null;
  terminado?: number | null;
  iteracion?: IIteracion | null;
  alumno?: IAlumno | null;
}

export class IteracionAlumno implements IIteracionAlumno {
  constructor(
    public id?: number,
    public actividad?: string | null,
    public terminado?: number | null,
    public iteracion?: IIteracion | null,
    public alumno?: IAlumno | null
  ) {}
}

export function getIteracionAlumnoIdentifier(iteracionAlumno: IIteracionAlumno): number | undefined {
  return iteracionAlumno.id;
}
