import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IIteracion } from '../iteracion.model';
import { IteracionService } from '../service/iteracion.service';
import { IteracionDeleteDialogComponent } from '../delete/iteracion-delete-dialog.component';

@Component({
  selector: 'jhi-iteracion',
  templateUrl: './iteracion.component.html',
})
export class IteracionComponent implements OnInit {
  iteracions?: IIteracion[];
  isLoading = false;

  constructor(protected iteracionService: IteracionService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.iteracionService.query().subscribe({
      next: (res: HttpResponse<IIteracion[]>) => {
        this.isLoading = false;
        this.iteracions = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IIteracion): number {
    return item.id!;
  }

  delete(iteracion: IIteracion): void {
    const modalRef = this.modalService.open(IteracionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.iteracion = iteracion;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
