import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IIteracion, Iteracion } from '../iteracion.model';

import { IteracionService } from './iteracion.service';

describe('Iteracion Service', () => {
  let service: IteracionService;
  let httpMock: HttpTestingController;
  let elemDefault: IIteracion;
  let expectedResult: IIteracion | IIteracion[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(IteracionService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      fechaInicio: currentDate,
      fechaFin: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          fechaInicio: currentDate.format(DATE_FORMAT),
          fechaFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Iteracion', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          fechaInicio: currentDate.format(DATE_FORMAT),
          fechaFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
        },
        returnedFromService
      );

      service.create(new Iteracion()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Iteracion', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          fechaInicio: currentDate.format(DATE_FORMAT),
          fechaFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Iteracion', () => {
      const patchObject = Object.assign(
        {
          fechaInicio: currentDate.format(DATE_FORMAT),
        },
        new Iteracion()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Iteracion', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          fechaInicio: currentDate.format(DATE_FORMAT),
          fechaFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Iteracion', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addIteracionToCollectionIfMissing', () => {
      it('should add a Iteracion to an empty array', () => {
        const iteracion: IIteracion = { id: 123 };
        expectedResult = service.addIteracionToCollectionIfMissing([], iteracion);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iteracion);
      });

      it('should not add a Iteracion to an array that contains it', () => {
        const iteracion: IIteracion = { id: 123 };
        const iteracionCollection: IIteracion[] = [
          {
            ...iteracion,
          },
          { id: 456 },
        ];
        expectedResult = service.addIteracionToCollectionIfMissing(iteracionCollection, iteracion);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Iteracion to an array that doesn't contain it", () => {
        const iteracion: IIteracion = { id: 123 };
        const iteracionCollection: IIteracion[] = [{ id: 456 }];
        expectedResult = service.addIteracionToCollectionIfMissing(iteracionCollection, iteracion);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iteracion);
      });

      it('should add only unique Iteracion to an array', () => {
        const iteracionArray: IIteracion[] = [{ id: 123 }, { id: 456 }, { id: 5394 }];
        const iteracionCollection: IIteracion[] = [{ id: 123 }];
        expectedResult = service.addIteracionToCollectionIfMissing(iteracionCollection, ...iteracionArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const iteracion: IIteracion = { id: 123 };
        const iteracion2: IIteracion = { id: 456 };
        expectedResult = service.addIteracionToCollectionIfMissing([], iteracion, iteracion2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iteracion);
        expect(expectedResult).toContain(iteracion2);
      });

      it('should accept null and undefined values', () => {
        const iteracion: IIteracion = { id: 123 };
        expectedResult = service.addIteracionToCollectionIfMissing([], null, iteracion, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iteracion);
      });

      it('should return initial array if no Iteracion is added', () => {
        const iteracionCollection: IIteracion[] = [{ id: 123 }];
        expectedResult = service.addIteracionToCollectionIfMissing(iteracionCollection, undefined, null);
        expect(expectedResult).toEqual(iteracionCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
