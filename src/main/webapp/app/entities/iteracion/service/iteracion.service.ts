import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IIteracion, getIteracionIdentifier } from '../iteracion.model';

export type EntityResponseType = HttpResponse<IIteracion>;
export type EntityArrayResponseType = HttpResponse<IIteracion[]>;

@Injectable({ providedIn: 'root' })
export class IteracionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/iteracions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(iteracion: IIteracion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iteracion);
    return this.http
      .post<IIteracion>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(iteracion: IIteracion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iteracion);
    return this.http
      .put<IIteracion>(`${this.resourceUrl}/${getIteracionIdentifier(iteracion) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(iteracion: IIteracion): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iteracion);
    return this.http
      .patch<IIteracion>(`${this.resourceUrl}/${getIteracionIdentifier(iteracion) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIteracion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIteracion[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addIteracionToCollectionIfMissing(
    iteracionCollection: IIteracion[],
    ...iteracionsToCheck: (IIteracion | null | undefined)[]
  ): IIteracion[] {
    const iteracions: IIteracion[] = iteracionsToCheck.filter(isPresent);
    if (iteracions.length > 0) {
      const iteracionCollectionIdentifiers = iteracionCollection.map(iteracionItem => getIteracionIdentifier(iteracionItem)!);
      const iteracionsToAdd = iteracions.filter(iteracionItem => {
        const iteracionIdentifier = getIteracionIdentifier(iteracionItem);
        if (iteracionIdentifier == null || iteracionCollectionIdentifiers.includes(iteracionIdentifier)) {
          return false;
        }
        iteracionCollectionIdentifiers.push(iteracionIdentifier);
        return true;
      });
      return [...iteracionsToAdd, ...iteracionCollection];
    }
    return iteracionCollection;
  }

  protected convertDateFromClient(iteracion: IIteracion): IIteracion {
    return Object.assign({}, iteracion, {
      fechaInicio: iteracion.fechaInicio?.isValid() ? iteracion.fechaInicio.format(DATE_FORMAT) : undefined,
      fechaFin: iteracion.fechaFin?.isValid() ? iteracion.fechaFin.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaInicio = res.body.fechaInicio ? dayjs(res.body.fechaInicio) : undefined;
      res.body.fechaFin = res.body.fechaFin ? dayjs(res.body.fechaFin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((iteracion: IIteracion) => {
        iteracion.fechaInicio = iteracion.fechaInicio ? dayjs(iteracion.fechaInicio) : undefined;
        iteracion.fechaFin = iteracion.fechaFin ? dayjs(iteracion.fechaFin) : undefined;
      });
    }
    return res;
  }
}
