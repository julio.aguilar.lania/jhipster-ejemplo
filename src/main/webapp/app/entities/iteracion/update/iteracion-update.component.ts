import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IIteracion, Iteracion } from '../iteracion.model';
import { IteracionService } from '../service/iteracion.service';

@Component({
  selector: 'jhi-iteracion-update',
  templateUrl: './iteracion-update.component.html',
})
export class IteracionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    fechaInicio: [null, [Validators.required]],
    fechaFin: [null, [Validators.required]],
  });

  constructor(protected iteracionService: IteracionService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ iteracion }) => {
      this.updateForm(iteracion);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const iteracion = this.createFromForm();
    if (iteracion.id !== undefined) {
      this.subscribeToSaveResponse(this.iteracionService.update(iteracion));
    } else {
      this.subscribeToSaveResponse(this.iteracionService.create(iteracion));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIteracion>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(iteracion: IIteracion): void {
    this.editForm.patchValue({
      id: iteracion.id,
      fechaInicio: iteracion.fechaInicio,
      fechaFin: iteracion.fechaFin,
    });
  }

  protected createFromForm(): IIteracion {
    return {
      ...new Iteracion(),
      id: this.editForm.get(['id'])!.value,
      fechaInicio: this.editForm.get(['fechaInicio'])!.value,
      fechaFin: this.editForm.get(['fechaFin'])!.value,
    };
  }
}
