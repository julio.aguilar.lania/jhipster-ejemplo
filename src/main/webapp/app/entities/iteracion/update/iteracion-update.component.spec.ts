import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IteracionService } from '../service/iteracion.service';
import { IIteracion, Iteracion } from '../iteracion.model';

import { IteracionUpdateComponent } from './iteracion-update.component';

describe('Iteracion Management Update Component', () => {
  let comp: IteracionUpdateComponent;
  let fixture: ComponentFixture<IteracionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let iteracionService: IteracionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [IteracionUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(IteracionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IteracionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    iteracionService = TestBed.inject(IteracionService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const iteracion: IIteracion = { id: 456 };

      activatedRoute.data = of({ iteracion });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(iteracion));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iteracion>>();
      const iteracion = { id: 123 };
      jest.spyOn(iteracionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iteracion }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(iteracionService.update).toHaveBeenCalledWith(iteracion);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iteracion>>();
      const iteracion = new Iteracion();
      jest.spyOn(iteracionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iteracion }));
      saveSubject.complete();

      // THEN
      expect(iteracionService.create).toHaveBeenCalledWith(iteracion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iteracion>>();
      const iteracion = { id: 123 };
      jest.spyOn(iteracionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iteracion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(iteracionService.update).toHaveBeenCalledWith(iteracion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
