import dayjs from 'dayjs/esm';

export interface IIteracion {
  id?: number;
  fechaInicio?: dayjs.Dayjs;
  fechaFin?: dayjs.Dayjs;
}

export class Iteracion implements IIteracion {
  constructor(public id?: number, public fechaInicio?: dayjs.Dayjs, public fechaFin?: dayjs.Dayjs) {}
}

export function getIteracionIdentifier(iteracion: IIteracion): number | undefined {
  return iteracion.id;
}
