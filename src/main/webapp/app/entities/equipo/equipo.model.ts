export interface IEquipo {
  id?: number;
  nombre?: string;
}

export class Equipo implements IEquipo {
  constructor(public id?: number, public nombre?: string) {}
}

export function getEquipoIdentifier(equipo: IEquipo): number | undefined {
  return equipo.id;
}
